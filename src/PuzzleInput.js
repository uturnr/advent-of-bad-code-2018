import React from 'react';
import { Alert, Button, Form, FormGroup, Label, Input } from 'reactstrap';

class PuzzleInput extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      inputData: '',
    };
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.setStartPuzzle(false);
    this.props.setCustomPuzzleInput(this.state.inputData);
  }

  render() {

    let content;

    if (this.props.customPuzzleInput) {

      content = (
        <Alert className="mt-4" color="primary">
          <p>{`You are using a Custom Puzzle Input. `}</p>
          <hr />
          <div>
            <Button
              color="primary"
              onClick={() => {
                this.props.setStartPuzzle(false);
                this.props.setCustomPuzzleInput(null);
              }}
              size="sm"
            >
              Edit Input
            </Button>
            {' '}
            <Button
              color="primary"
              onClick={() => {
                this.props.setStartPuzzle(false);
                this.props.setCustomPuzzleInput(null);
                this.props.setCustomPuzzleInputsOn(false);
              }}
              size="sm"
            >
              Use Cody's Input
            </Button>
          </div>
        </Alert>
      );

    } else {

      content = (
        <Form className="mt-4" onSubmit={e => this.handleSubmit(e)}>
          <FormGroup>
            <Label for="exampleText">Your Puzzle Input</Label>
            <Input
              className="PuzzleInput__textarea"
              id="exampleText"
              name="text"
              onChange={e => this.setState({ inputData: e.target.value })}
              type="textarea"
              value={this.state.inputData}
            />
          </FormGroup>
          <Button>Submit</Button>
        </Form>
      );

    }

    return content;

  }

}

export default PuzzleInput;
