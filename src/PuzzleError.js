import React from 'react';
import { Alert } from 'reactstrap';

class PuzzleError extends React.Component {

  render() {

    const messages = [
      'Perhaps there was a problem with your custom puzzle input.',
      'My apologies!'
    ];

    return (
      <div className="PuzzleError mt-4">
        <Alert color="danger">
          {`There was an error! `}
          {`${this.props.customPuzzleInput ? messages[0] : messages[1]} `}
        </Alert>
      </div>
    );

  }

}

export default PuzzleError;
