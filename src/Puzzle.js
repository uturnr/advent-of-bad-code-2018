import React from 'react';
import queryString from 'query-string';
import PuzzleInput from './PuzzleInput';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import { FaArrowLeft } from 'react-icons/fa';

const preambles = {
  Year2018Day9Part2: 'I started looking at making my solution efficient, and then just decided to let it run overnight. It took many hours. Click start at your own risk.',
  Year2018Day11Part2: 'This one takes a while. Here, take this progress bar: ⏳. Also, if you navigate away, the site’s going to crash. I’m too lazy to cancel my promises. ⚰️',
};

class Puzzle extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      customPuzzleInput: null,
      started: false,
    };
  }

  setCustomPuzzleInput(customPuzzleInput) {
    this.setState({
      customPuzzleInput,
    });
  }

  setStartPuzzle(bool) {
    this.setState({ started: bool });
  }

  render() {

    const currentPuzzleStr = queryString.parse(this.props.location.search).id;
    const currentPuzzleModule = require(`./solutions/${currentPuzzleStr}`);
    const CurrentPuzzle = currentPuzzleModule.default;
    const puzzleNameRegex = /Year(.*?)Day(.*?)Part(.*?)$/g;
    const result = puzzleNameRegex.exec(currentPuzzleStr);
    const year = result[1];
    const day = result[2];
    const part = result[3];
    const puzzleDisplayName = `Year ${year}, Day ${day}, Part ${part}`;

    let puzzleInput;
    let startButton;
    const preambleStr = preambles[currentPuzzleStr];
    const preamble = preambleStr ? <p>{preambleStr}</p> : null;
    const startButtonTemplate = (
      <div className="mt-4">
        {preamble}
        <Button onClick={() => this.setStartPuzzle(true)}>Start</Button>
      </div>
    );
    let currentPuzzleComponent = <CurrentPuzzle
      customPuzzleInput={this.state.customPuzzleInput}
      currentPuzzleStr={currentPuzzleStr}
    />;
    let currentPuzzle = currentPuzzleComponent;

    if (this.props.customPuzzleInputsOn) {
      puzzleInput = <PuzzleInput
        customPuzzleInput={this.state.customPuzzleInput}
        setCustomPuzzleInput={input => this.setCustomPuzzleInput(input)}
        setCustomPuzzleInputsOn={this.props.setCustomPuzzleInputsOn}
        setStartPuzzle={bool => this.setStartPuzzle(bool)}
      />;
      if (!this.state.customPuzzleInput) {
        currentPuzzle = null;
      } else {
        currentPuzzle = currentPuzzleComponent;
      }
    }

    if (
      !this.state.started
      && (
        !this.props.customPuzzleInputsOn
        || (
          this.props.customPuzzleInputsOn
          && this.state.customPuzzleInput
        )
      )
    ) {
      currentPuzzle = null;
      startButton = startButtonTemplate;
    }

    return (
      <div className="Puzzle">
        <div className="text-left">
          <h1>{puzzleDisplayName}</h1>
          <Link to={`${process.env.PUBLIC_URL}/`}><FaArrowLeft /><span className="Puzzle__back__text">Back</span></Link>
        </div>
        {puzzleInput}
        {startButton}
        {currentPuzzle}
      </div>
    );

  }

}

export default Puzzle
