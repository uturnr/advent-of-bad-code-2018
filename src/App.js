import React, { Component } from 'react';
import './App.css';
import Header from './Header';
import Main from './Main';
import 'bootstrap/dist/css/bootstrap.min.css';
import { hot } from 'react-hot-loader'


class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      customPuzzleInputsOn: false,
    };
  }

  setCustomPuzzleInputsOn(bool) {
    this.setState({
      customPuzzleInputsOn: bool,
    });
  }

  render() {

    return (
      <div className="App">
        <Header
          customPuzzleInputsOn={this.state.customPuzzleInputsOn}
          setCustomPuzzleInputsOn={bool => this.setCustomPuzzleInputsOn(bool)}
        />
        <Main
          customPuzzleInputsOn={this.state.customPuzzleInputsOn}
          setCustomPuzzleInputsOn={bool => this.setCustomPuzzleInputsOn(bool)}
        />
      </div>
    );
  }
}

export default hot(module)(App);
