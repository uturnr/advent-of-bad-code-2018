import React from 'react';

import PuzzleError from '../PuzzleError';
import { Jumbotron } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);
    
    this.state = {
      hasError: false,
      answer: 'Thinking...'
    };

    this.array = [];
    this.input = 5;
    this.output = 0;
    this.position = 0;

  }

  componentDidCatch() {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;
    /** @type string */
    // this.str = '3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99';
    this.str = data;

    this.parseDataAndRun();
    
  }

  parseDataAndRun() {
    this.array = this.str.split(',').map(Number);
    this.runInstruction();
  }

  getVal(paramMode, param) {
    switch(paramMode) {
      case 0: // position
        return this.array[param]
      case 1: // immediate (value)
        return param
      default:
        throw new Error('oops');
    }
  }

  runInstruction() {

    console.log(this.array.toString());
    const opcode = this.array[this.position].toString().padStart(5, '0');
    const instruction = Number(opcode.slice(-2));
    const paramModes = {
      1: Number(opcode[2]),
      2: Number(opcode[1]),
      3: Number(opcode[0]),
    };
    const params = {
      1: this.array[this.position + 1],
      2: this.array[this.position + 2],
      3: this.array[this.position + 3],
    }
    console.log('Instruction', instruction)
    switch(instruction) {
      case 1:
        this.runInst1(paramModes, params);
        break;
      case 2:
        this.runInst2(paramModes, params);
        break;
      case 3:
        this.runInst3(params);
        break;
      case 4:
        this.runInst4(paramModes, params);
        break;
      case 5:
        this.runInst5(paramModes, params);
        break;
      case 6:
        this.runInst6(paramModes, params);
        break;
      case 7:
        this.runInst7(paramModes, params);
        break;
      case 8:
        this.runInst8(paramModes, params);
        break;
      case 99:
        this.runInst99();
        break;
      default:
        debugger;
        // throw new Error('oops');
    }
  }

  runInst1(paramModes, params) { // sum
    const x1 = this.getVal(paramModes[1], params[1]);
    const x2 = this.getVal(paramModes[2], params[2]);
    const writePos = params[3];
    this.array[writePos] = x1 + x2;
    this.position += 4;
    this.runInstruction();
  }

  runInst2(paramModes, params) { // mult
    const x1 = this.getVal(paramModes[1], params[1]);
    const x2 = this.getVal(paramModes[2], params[2]);
    const writePos = params[3];
    this.array[writePos] = x1 * x2;
    this.position += 4;
    this.runInstruction();
  }

  runInst3(params) { // write input
    this.array[params[1]] = this.input;
    this.position += 2;
    this.runInstruction();
  }

  runInst4(paramModes, params) { // output value
    const value = this.getVal(paramModes[1], params[1]);
    this.output = value;
    this.position += 2;
    this.runInstruction();
  }

  runInst5(paramModes, params) { // jump if true
    const maybeZero = this.getVal(paramModes[1], params[1]);
    if (maybeZero !== 0) {
      this.position = this.getVal(paramModes[2], params[2]);
    } else {
      this.position += 3;
    }
    this.runInstruction();
  }

  runInst6(paramModes, params) { // jump if false
    const maybeZero = this.getVal(paramModes[1], params[1]);
    if (maybeZero === 0) {
      this.position = this.getVal(paramModes[2], params[2]);
    } else {
      this.position += 3;
    }
    this.runInstruction();
  }

  runInst7(paramModes, params) { // less than
    const x1 = this.getVal(paramModes[1], params[1]);
    const x2 = this.getVal(paramModes[2], params[2]);
    const writePos = params[3];
    this.array[writePos] = x1 < x2 ? 1 : 0;
    this.position += 4;
    this.runInstruction();
  }

  runInst8(paramModes, params) { // equals
    const x1 = this.getVal(paramModes[1], params[1]);
    const x2 = this.getVal(paramModes[2], params[2]);
    const writePos = params[3];
    this.array[writePos] = x1 === x2 ? 1 : 0;
    this.position += 4;
    this.runInstruction();
  }

  runInst99() {
    this.setState({
      answer: this.output
    });
  }

  componentDidMount() {

    if (process.env.NODE_ENV === 'production') {
      try {
        this.runSolution();
      } catch(err) {
        console.error(err);
        this.setState({ hasError: true });
      }
    } else {
      this.runSolution();
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <Jumbotron className="Solution__Jumbotron my-4">
          <h3>What diagnostic code does the program produce?</h3>
          <hr className="my-2" />
          <div>{this.state.answer}</div>
        </Jumbotron>
      </div>
    );

  }

}

export default CurrentSolution;
