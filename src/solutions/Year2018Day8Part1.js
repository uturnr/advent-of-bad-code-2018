import React from 'react';

import PuzzleError from '../PuzzleError';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      content: '',
    };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;
    const dataArr = data.split(' ').map(Number);
    console.log(dataArr);

    const levels = [];
    const nodes = {};

    let currentLevel = 0;
    levels[currentLevel] = [];
    let currentNodeId = 0;
    let highestNodeId = 0;
    let currentParentId = null;

    const addShell = (childCount, metaCount, parentId) => {

      const shell = {
        childCount,
        childrenRemaining: childCount,
        level: currentLevel,
        metaCount,
        meta: null,
        parentId,
      };
      levels[currentLevel].push(shell);
      nodes[currentNodeId] = shell;

    }


    let i = 0;
    let merry = 'christmas';

    while (merry === 'christmas') {

      let currentNode = nodes[currentNodeId];

      if (!currentNode) {

        // init
        const childCount = dataArr[0];
        const metaCount = dataArr[1];
        addShell(childCount, metaCount, currentParentId);

      } else if (currentNode.childrenRemaining) {

        // console.log('node has children to process');
        currentLevel++;
        currentNode.childrenRemaining--;
        if (!levels[currentLevel]) levels[currentLevel] = [];
        currentParentId = currentNodeId;
        highestNodeId++;
        currentNodeId = highestNodeId;
        i += 2;
        const childCount = dataArr[0 + i];
        const metaCount = dataArr[1 + i];
        addShell(childCount, metaCount, currentParentId);

      } else {

        // console.log('node is out of children');
        i += 2;
        const meta = dataArr.slice(i, i + currentNode.metaCount);
        i += currentNode.metaCount;
        currentNode.meta = meta;
        const parentNodeId = nodes[currentNodeId].parentId;
        if (parentNodeId === null) {
          console.log('momma node is out of children, all done.');
          break;
        }
        if (!nodes[parentNodeId].childrenRemaining) {
          // console.log('nodes parent is out of children, back to parent');
          currentLevel--;
          currentNodeId = parentNodeId;
          currentNode = nodes[currentNodeId];
          currentParentId = nodes[currentNodeId].parentId;
          i -= 2; // step back 2 to "neutralize" with upcoming added 2
        } else {
          // console.log('nodes parent still has children, on to the next');
          nodes[parentNodeId].childrenRemaining--;
          highestNodeId++;
          currentNodeId = highestNodeId;
          const childCount = dataArr[0 + i];
          const metaCount = dataArr[1 + i];
          addShell(childCount, metaCount, currentParentId);
        }

      }

    }

    console.log(nodes);



    let metaTotal = 0;

    for (let node in nodes) {
      const metaArr = nodes[node].meta;
      for (let num of metaArr) {
        metaTotal += num;
      }
    }

    let content = `What is the sum of all metadata entries? ${metaTotal}!`


    this.setState({
      content
    });

  }

  componentDidMount() {

    try {
      this.runSolution();
    } catch(err) {
      this.setState({ hasError: true });
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <div className="my-4 monospace">
          {this.state.content}
        </div>
      </div>
    );

  }

}

export default CurrentSolution;
