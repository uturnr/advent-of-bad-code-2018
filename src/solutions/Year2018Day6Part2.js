import React from 'react';
import PuzzleError from '../PuzzleError';
import { Jumbotron } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      content: '',
    };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const emojis = codysData.emojis;
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;
    const dataArr = data.split('\n');

    // get all x, y pairs

    const coords = [];
    let xLow;
    let xHigh;
    let yLow;
    let yHigh;

    for (let string of dataArr) {
      const xyRegex = /(.*?), (.*?)$/g;
      const xy = xyRegex.exec(string);
      const x = Number(xy[1]);
      const y = Number(xy[2]);
      coords.push([x,y]);
      if (!xLow || x < xLow) xLow = x;
      if (!xHigh || x > xHigh) xHigh = x;
      if (!yLow || y < yLow) yLow = y;
      if (!yHigh || y > yHigh) yHigh = y;
    }

    // set bounds of grid based on lowest/highest xy

    const width = xHigh - xLow;
    const height = yHigh - yLow;

    const yTemplate = Array(width).fill(0); // columns
    const xyArray = Array(height).fill(null); // rows

    let adjCoords = [];

    coords.forEach((pair, i) => {
      const adjX = pair[0] - xLow;
      const adjY = pair[1] - yLow;
      const adjPair = [adjX, adjY];
      adjCoords.push(adjPair);
    });

    // check each coordinate within grid and find closest pair

    let elevenCount = 0;

    xyArray.forEach((yArray, yIndex) => {
      if (yArray === null) {
        xyArray[yIndex] = yTemplate.slice(0);
      }
      xyArray[yIndex].forEach((value, xIndex) => {
        const totalDiffs = [];
        adjCoords.forEach((pair, coordIndex) => {
          let xDiff = pair[0] - xIndex;
          xDiff = -xDiff > 0 ? -xDiff : xDiff; // make positive
          let yDiff = pair[1] - yIndex;
          yDiff = -yDiff > 0 ? -yDiff : yDiff; // make positive
          const totalDiff = xDiff + yDiff;
          totalDiffs.push(totalDiff);
        });
        let totalDistance = 0;
        totalDiffs.forEach(value => {
          totalDistance += value;
        })
        // 11 less than 10000
        // 88 10000+
        let flag = 88;
        if (totalDistance < 10000) {
          flag = 11;
          elevenCount++;
        }
        xyArray[yIndex][xIndex] = flag;
      });
    });

    // display

    let parsed = [];

    xyArray.forEach((yArray, yIndex) => {
      let row = [];
      xyArray[yIndex].forEach((value, xIndex) => {
        row.push(emojis[value]);
        // `${value.toString().padStart(2, '0')}`
      });
      parsed.push(<div key={yIndex}>{row}</div>);
    });

    const result = `${elevenCount}!!!`;

    this.setState({
      content: [
        parsed,
        result,
      ]
    });

  }

  componentDidMount() {

    try {
      this.runSolution();
    } catch(err) {
      console.log(err);
      this.setState({ hasError: true });
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <Jumbotron className="my-4 Solution__Jumbotron">
          <h3>
            What is the size of the region containing all locations which have a total distance to all given coordinates of less than 10000???
          </h3>
          <hr className="my-2" />
          <div>{this.state.content[1]}</div>
        </Jumbotron>
        <pre className="my-4 monospace font-small">
          {this.state.content[0]}
        </pre>
      </div>
    );

  }

}

export default CurrentSolution;
