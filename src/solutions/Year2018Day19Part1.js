import React from 'react';

import PuzzleError from '../PuzzleError';
import { Jumbotron, Table } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);
    
    this.state = {
      hasError: false,
    };

    this.ops = {
      
      addr: (reg, i) => {
        reg[i[3]] = reg[i[1]] + reg[i[2]];
      },

      addi: (reg, i) => {
        reg[i[3]] = reg[i[1]] + i[2];
      },

      mulr: (reg, i) => {
        reg[i[3]] = reg[i[1]] * reg[i[2]];
      },

      muli: (reg, i) => {
        reg[i[3]] = reg[i[1]] * i[2];
      },

      banr: (reg, i) => {
        reg[i[3]] = reg[i[1]] & reg[i[2]];
      },

      bani: (reg, i) => {
        reg[i[3]] = reg[i[1]] & i[2];
      },
      
      borr: (reg, i) => {
        reg[i[3]] = reg[i[1]] | reg[i[2]];
      },

      bori: (reg, i) => {
        reg[i[3]] = reg[i[1]] | i[2];
      },
      
      setr: (reg, i) => {
        reg[i[3]] = reg[i[1]];
      },

      seti: (reg, i) => {
        reg[i[3]] = i[1];
      },
      
      gtir: (reg, i) => {
        reg[i[3]] = i[1] > reg[i[2]] ? 1 : 0;
      },

      gtri: (reg, i) => {
        reg[i[3]] = reg[i[1]] > i[2] ? 1 : 0;
      },
      
      gtrr: (reg, i) => {
        reg[i[3]] = reg[i[1]] > reg[i[2]] ? 1 : 0;
      },
      
      eqir: (reg, i) => {
        reg[i[3]] = i[1] === reg[i[2]] ? 1 : 0;
      },

      eqri: (reg, i) => {
        reg[i[3]] = reg[i[1]] === i[2] ? 1 : 0;
      },
      
      eqrr: (reg, i) => {
        reg[i[3]] = reg[i[1]] === reg[i[2]] ? 1 : 0;
      },

    };

    this.register = [0,0,0,0,0,0];
    this.ipVal = 0;
    this.instructions = [];
    this.potentialPatterns = {};
    this.currentPattern = null;
    this.step = 0;
    this.tableRows = [];
    this.logs = {};
  }

  componentDidCatch() {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;
// const data = `#ip 0
// seti 5 0 1
// seti 6 0 2
// addi 0 1 0
// addr 1 2 3
// setr 1 0 0
// seti 8 0 4
// seti 9 0 5`;

    const dataArr = data.split('\n');
    this.parseData(dataArr);
    this.runProgram();
    
  }

  parseData(dataArr) {
    for (let i  = 0; i < dataArr.length; i++) {
      /** @type {string} */
      const line = dataArr[i];
      if (i === 0) {
        this.ipBoundTo = Number(line.substring(4, line.length));
      } else {
        const result = /(.+) (.+) (.+) (.+)/.exec(line);
        this.instructions.push({
          opname: result[1],
          i: [result[1], Number(result[2]), Number(result[3]), Number(result[4])],
        });
      }
    }
  }

  runProgram() {
    while (!this.programHalted) {      
      // if (this.step > 15631) this.programHalted = true;
      const tableIP = this.ipVal.toString();
      const instruction = this.instructions[this.ipVal];
      this.register[this.ipBoundTo] = this.ipVal; // When the instruction pointer is bound to a register, its value is written to that register just before each instruction is executed
      const tableBefore = this.register.toString();
      const tableOp = `${instruction.opname} ${instruction.i.slice(-3).toString()}`;
      this.ops[instruction.opname](this.register, instruction.i); // run the instruction
      const tableAfter = this.register.toString();
      this.ipVal = this.register[this.ipBoundTo] + 1; // the value of that register is written back to the instruction pointer immediately after each instruction finishes execution. Afterward, move to the next instruction by adding one to the instruction pointer.
      this.tableRows.push(
        <tr key={this.tableRows.length}>
          <td>{this.step}</td>
          <td>{tableIP}</td>
          <td>{tableBefore}</td>
          <td>{tableOp}</td>
          <td>{tableAfter}</td>
        </tr>
      );
      this.logs[this.step] = { tableIP, tableBefore, tableOp, tableAfter };
      this.findPattern(tableIP, this.step);
      if (this.ipVal >= this.instructions.length) {
        this.programHalted = true;
        this.setState({ 
          tableRows: this.tableRows,
          answer: this.register[0],
        });
      } else {
        this.step++;
      }
    }
  }

  findPattern(ipVal, step) {

    const pp = this.potentialPatterns;
    
    if (!(ipVal in pp)) {
      pp[ipVal] = { 
        pattern: [],
        matchesFound: 0,
        findingMatch: false,
        pLength: null,
        index: step,
      };
    }
    for (const ipKey in pp) {
      if (
        ipKey === ipVal
        && pp[ipKey].pattern.length
      ) { // a number has been repeated
        if (!pp[ipKey].findingMatch) { //2nd instance of starting number
          pp[ipKey].findingMatch = 1;
          pp[ipKey].pLength = pp[ipKey].pattern.length;
        } else if (pp[ipKey].findingMatch === 1) { //3rd instance of starting number
          pp[ipKey].findingMatch = 2;
        }
      }
      
      pp[ipKey].pattern.push(ipVal);
      if (pp[ipKey].findingMatch) {
        // check if added character matches
        const matchableI = pp[ipKey].pattern.length - pp[ipKey].pLength * pp[ipKey].findingMatch - 1;
        if (ipVal === pp[ipKey].pattern[matchableI]) { // char match
          if (pp[ipKey].pattern.length === pp[ipKey].pLength * (pp[ipKey].findingMatch + 1)) { // full match
            pp[ipKey].matchesFound++;
            if (pp[ipKey].matchesFound === 2) {
              this.programHalted = true;
              this.currentPattern = {
                start: pp[ipKey].index,
                pattern: pp[ipKey].pattern.slice(0, pp[ipKey].pLength),
              };
              this.potentialPatterns = {};
              this.evaluateLoop();
            }
          }
        } else { // no match, reset
          pp[ipVal] = { 
            pattern: [ipVal],
            matchesFound: 0,
            maybeMatch: false,
            pLength: null,
            index: step,
          };
        }
        
      }

    }
  }

  evaluateLoop() {
    const start = this.currentPattern.start;
    const pLength = this.currentPattern.pattern.length;
    const waysToBreak = [];
    for (let i = 0; i < pLength; i++) {
      const ip = this.currentPattern.pattern[i];
      const opname = this.instructions[ip].opname;
      if (opname === 'gtrr' || opname === 'eqrr') {
        // get three events for each.
        const instruction = this.instructions[ip].i;
        const before1 = this.logs[start + i].tableBefore.split(',').map(Number);
        const before2 = this.logs[start + pLength + i].tableBefore.split(',').map(Number);
        const before3 = this.logs[start + pLength * 2 + i].tableBefore.split(',').map(Number);
        const samples = [before1, before2, before3];
        const stepsToBreak = this.getStepsToBreakLoop(instruction, samples, opname, i, pLength);
        if (stepsToBreak) waysToBreak.push(stepsToBreak);
      }
    }
    waysToBreak.sort((a, b) => a.loopSteps - b.loopSteps);
    // console.log(waysToBreak);
    // console.log(start + waysToBreak[0].loopSteps);
    if (waysToBreak[0].loopSteps < 0) {
      debugger;
    }
    this.breakLoop(start, waysToBreak[0]);
    

    
  }

  getStepsToBreakLoop(instruction, samples, opname, indexWithinLoop, pLength) {
    let opsByReg = [];
    let opsByRegStr = [];
    let factorsByReg = [];
    for (let i = 0; i < samples[0].length; i++) {
      const one = samples[0][i];
      const two = samples[1][i];
      const three = samples[2][i];

      if (one === two) {
        opsByReg.push(num => num); // fixed
        opsByRegStr.push('fixed');
        factorsByReg.push(0);
      } else if (three - two === two - one) {
        opsByReg.push((num, steps) => num + (two - one) * steps); // fixed sum
        opsByRegStr.push('sum');
        factorsByReg.push(two - one);
      } else if (three / two === two / one) {
        opsByReg.push((num, steps) => num * Math.pow((two / one), steps)); // fixed mult
        opsByRegStr.push('mult');
        console.log('multiplying');
        
        factorsByReg.push(two / one);
      } else {
        console.log(samples);
        
        throw new Error('An unexpected operation is performed in the loop.');
      }
    }

    // how many loops until the result will change
    const inputA = instruction[1];
    const inputAOp = opsByRegStr[inputA];
    const inputB = instruction[2];
    const inputBOp = opsByRegStr[inputB];
    let watchReg;
    let fixedReg;
    let watchOp;
    if (inputAOp === 'fixed' && inputBOp !== 'fixed') {
      fixedReg = inputA;
      watchReg = inputB;
      watchOp = inputBOp;
    } else if (inputAOp !== 'fixed' && inputBOp === 'fixed') {
      fixedReg = inputB;
      watchReg = inputA;
      watchOp = inputAOp;
    } else {
      throw new Error('Fiddlesticks!');
    }

    const watchVal = samples[0][watchReg];
    const watchVal2 = samples[1][watchReg];
    const fixedVal = samples[0][fixedReg];
    const diff = Math.abs(watchVal - fixedVal);
    const diff2 = Math.abs(watchVal2 - fixedVal);
    if (diff <= diff2) { // not getting closer, get out of here
      return;
    }
    const factor = factorsByReg[watchReg];

    const adj = opname === 'gtrr' ? 1 : 0;

    let selfLoops;
    if (watchOp === 'sum') {
      selfLoops = Math.abs((fixedVal + adj - watchVal) / factor);
    } else if (watchOp === 'mult') {
      selfLoops = Math.ceil(Math.log((fixedVal + adj) / watchVal) / Math.log(factor));
    }

    if (!Number.isInteger(selfLoops)) return; // can't do eqrr if the number skips over

    return {
      before: samples[0],
      selfSteps: selfLoops * pLength,
      loopSteps: selfLoops * pLength + indexWithinLoop,
      loops: selfLoops,
      opname,
      opsByReg,
    };


  }

  breakLoop(start, wayToBreak) {
    this.tableRows.push(
      <tr key={this.tableRows.length}>
        <td></td>
        <td>TIME WARP</td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    );
    this.step = start + wayToBreak.loopSteps;
    const oldBeforeReg = wayToBreak.before;
    const beforeReg = [];
    for (let i = 0; i < wayToBreak.opsByReg.length; i++) {
      const specialOp = wayToBreak.opsByReg[i];
      const numFromBefore = oldBeforeReg[i];
      beforeReg.push(specialOp(numFromBefore, wayToBreak.loops));
    }
    this.register = beforeReg;
    this.ipVal = this.register[this.ipBoundTo];
    const tableIP = this.ipVal.toString();
    const instruction = this.instructions[this.ipVal];
    // this.register[this.ipBoundTo] = this.ipVal; // When the instruction pointer is bound to a register, its value is written to that register just before each instruction is executed
    const tableBefore = this.register.toString();
    const tableOp = `${instruction.opname} ${instruction.i.slice(-3).toString()}`;

    this.ops[instruction.opname](this.register, instruction.i); // run the instruction

    const tableAfter = this.register.toString();
    this.ipVal = this.register[this.ipBoundTo] + 1; // the value of that register is written back to the instruction pointer immediately after each instruction finishes execution. Afterward, move to the next instruction by adding one to the instruction pointer.
    this.tableRows.push(
      <tr key={this.tableRows.length}>
        <td>{this.step}</td>
        <td>{tableIP}</td>
        <td>{tableBefore}</td>
        <td>{tableOp}</td>
        <td>{tableAfter}</td>
      </tr>
    );
    this.logs[this.step] = { tableIP, tableBefore, tableOp, tableAfter };
    this.findPattern(tableIP, this.step);
    if (this.ipVal >= this.instructions.length) {
      this.programHalted = true;
      this.setState({ 
        tableRows: this.tableRows,
        answer: this.register[0],
      });
    } else {
      this.step++;
      this.programHalted = false;
      this.runProgram();
    }
  }

  componentDidMount() {

    if (process.env.NODE_ENV === 'production') {
      try {
        this.runSolution();
      } catch(err) {
        console.error(err);
        this.setState({ hasError: true });
      }
    } else {
      this.runSolution();
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <Jumbotron className="Solution__Jumbotron my-4">
          <h3>What value is left in register 0 when the background process halts?</h3>
          <hr className="my-2" />
          <div>{this.state.answer}</div>
        </Jumbotron>
        <Table className="my-4" size="sm" striped hover>
          <thead>
            <tr>
              <th>Index</th>
              <th>Instruction Pointer</th>
              <th>Before</th>
              <th>Operation</th>
              <th>After</th>
            </tr>
          </thead>
          <tbody>
            {this.state.tableRows}
          </tbody>
        </Table>

      </div>
    );

  }

}

export default CurrentSolution;
