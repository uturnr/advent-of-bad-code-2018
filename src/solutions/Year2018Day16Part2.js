import React from 'react';

import PuzzleError from '../PuzzleError';
import { Jumbotron, Table } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);
    
    this.state = {
      hasError: false,
      tableRows: [],
    };
    
    this.ops = {
      
      addr: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] + reg[i[2]];
        return newReg;
      },

      addi: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] + i[2];
        return newReg;
      },

      mulr: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] * reg[i[2]];
        return newReg;
      },

      muli: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] * i[2];
        return newReg;
      },

      banr: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] & reg[i[2]];
        return newReg;
      },

      bani: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] & i[2];
        return newReg;
      },
      
      borr: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] | reg[i[2]];
        return newReg;
      },

      bori: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] | i[2];
        return newReg;
      },
      
      setr: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]];
        return newReg;
      },

      seti: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = i[1];
        return newReg;
      },
      
      gtir: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = i[1] > reg[i[2]] ? 1 : 0;
        return newReg;
      },

      gtri: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] > i[2] ? 1 : 0;
        return newReg;
      },
      
      gtrr: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] > reg[i[2]] ? 1 : 0;
        return newReg;
      },
      
      eqir: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = i[1] === reg[i[2]] ? 1 : 0;
        return newReg;
      },

      eqri: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] === i[2] ? 1 : 0;
        return newReg;
      },
      
      eqrr: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] === reg[i[2]] ? 1 : 0;
        return newReg;
      },

    };
  }

  componentDidCatch() {
    this.setState({ hasError: true });
  }
  
  copyReg(reg) {
    return [reg[0], reg[1], reg[2], reg[3]];
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;

    const dataArr = data.split('\n');
    this.parseData(dataArr);
    
    this.evaluateAllSamplesBehaviour();
    this.determineOpcodes();
    this.executeTestProgram();

  }
  
  parseData(dataArr) {
    
    this.samples = [];
    
    let lastType;
    let sample = 0;
    
    const processResult = (result, type) => {
      result.shift()
      result = result.map(Number);
      this.samples[sample][type] = result;
      lastType = type;
    }
    
    let lastIndex;
    
    // section 1
    
    for (let i = 0; i < dataArr.length; i++) {
      const line = dataArr[i];
      const char0 = line.charAt(0);
      if (char0 === 'B') {
        this.samples[sample] = {};
        const result = /Before: \[(.+), (.+), (.+), (.+)\]/.exec(line);
        processResult(result, char0.toLowerCase());
      } else if (lastType === 'b' && char0.match(/\d/)) { // a number
        const result = /(.+) (.+) (.+) (.+)/.exec(line);
        processResult(result, 'i');
      } else if (char0 === 'A') {
        const result = /After: {2}\[(.+), (.+), (.+), (.+)\]/.exec(line);
        processResult(result, char0.toLowerCase());
      } else if (lastType === '' && char0.match(/\d/)) { // section 2 entered
        lastIndex = i;
        break;
      } else { // blank line
        sample++;
        lastType = '';
      }
    }
    
    this.instructions = [];
    
    const processResult2 = result => {
      result.shift()
      result = result.map(Number);
      this.instructions.push(result);
    }
    
    // section 2
    
    for (let i = lastIndex; i < dataArr.length; i++) {
      const line = dataArr[i];
      const result = /(.+) (.+) (.+) (.+)/.exec(line);
      processResult2(result);
    }
    
  }
  
  evaluateAllSamplesBehaviour() {

    for (let i = 0; i < this.samples.length; i++) {
      const sample = this.samples[i];
      const behavesLike = this.tryAllOps(sample);
      this.samples[i].behavesLike = behavesLike;
    }
    
  }
  
  determineOpcodes() {
    
    const opcodeData = [];
    for (let i = 0; i < 16; i++) {
      opcodeData[i] = {
        checks: 0,
        always: [],
      };
    }
    
    const takenOpNames = [];
    
    // eliminate opnames that are not present during every op
    
    for (let i = 0; i < this.samples.length; i++) {
      const sample = this.samples[i];
      const opcode = sample.i[0];
      const behavesLike = sample.behavesLike;
      const opcodeObj = opcodeData[opcode];
      
      if (opcodeObj.checks === 0) {
        opcodeObj.always = behavesLike;
        if (opcodeObj.always.length === 1) takenOpNames.push(opcodeObj.always[0]);
      } else if (opcodeObj.always.length !== 1) {
        for (let j = 0; j < opcodeObj.always.length; j++) {
          const alwaysOpName = opcodeObj.always[j];
          if (behavesLike.indexOf(alwaysOpName) === -1) {
            opcodeObj.always.splice(j, 1);
            if (opcodeObj.always.length === 1) takenOpNames.push(opcodeObj.always[0]);
          }
        }
      }
      opcodeObj.checks++;
    }
    
    // prune opnames that are spoken for
    
    const prune = () => {
      for (let i = 0; i < opcodeData.length; i++) {
        const opcodeObj = opcodeData[i];
        if (opcodeObj.always.length > 1) {
          for (let j = 0; j < takenOpNames.length; j++) {
            const indexOfTaken = opcodeObj.always.indexOf(takenOpNames[j]);
            if (indexOfTaken !== -1) {
              opcodeObj.always.splice(indexOfTaken, 1);
              if (opcodeObj.always.length === 1) {
                takenOpNames.push(opcodeObj.always[0]);
                break;
              } 
            }
          }
        }
        opcodeObj.checks++;
      }
      if (takenOpNames.length !== 16) prune();
    }
    
    prune();

    
    // build table and index
    
    const tableRows = [];
    const opcodes = [];
    
    for (let i = 0; i < opcodeData.length; i++) {
      tableRows.push(
        <tr key={`${i}`}>
          <td>{i}</td>
          <td>{opcodeData[i].checks}</td>
          <td>{opcodeData[i].always.toString()}</td>
        </tr>
      );
      opcodes.push(opcodeData[i].always.toString());
    }
    
    this.setState({ tableRows });
    this.opcodes = opcodes;
    
  }

  executeTestProgram() {

    this.register = [0,0,0,0];
    const table2Rows = [];

    for (let i = 0; i < this.instructions.length; i++) {
      const lastRegister = this.register.toString();
      const instruction = this.instructions[i];
      const opcode = instruction[0];
      const opname = this.opcodes[opcode];
      this.register = this.ops[opname](this.register,instruction);
      table2Rows.push(
        <tr key={`2-${i}`}>
          <td>{lastRegister}</td>
          <td>{instruction.toString()}</td>
          <td>{opname}</td>
          <td>{this.register.toString()}</td>
        </tr>
      );
    }

    const answer = this.register[0];

    this.setState({ table2Rows, answer });

  }
  
  tryAllOps(sample) {
    
    const behavesLike = [];
    for (const op in this.ops) {
      const after = this.ops[op](sample.b, sample.i);
      if (after.toString() === sample.a.toString()) {
        behavesLike.push(op);
      }
    }
    
    return behavesLike;
    
  }

  componentDidMount() {

    try {
      this.runSolution();
    } catch(err) {
      console.error(err);
      this.setState({ hasError: true });
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <Jumbotron className="Solution__Jumbotron my-4">
          <h3>What value is contained in register 0 after executing the test program?</h3>
          <hr className="my-2" />
          <div>{this.state.answer}</div>
        </Jumbotron>
        <Table className="my-4" size="sm" striped hover>
          <thead>
            <tr>
              <th>Opcode</th>
              <th>Checks</th>
              <th>Opname</th>
            </tr>
          </thead>
          <tbody>
            {this.state.tableRows}
          </tbody>
        </Table>
        <Table className="my-4" size="sm" striped hover>
          <thead>
            <tr>
              <th>Before</th>
              <th>Instruction</th>
              <th>Opname</th>
              <th>Result</th>
            </tr>
          </thead>
          <tbody>
            {this.state.table2Rows}
          </tbody>
        </Table>
      </div>
    );

  }

}

export default CurrentSolution;
