import React from 'react';

import PuzzleError from '../PuzzleError';
import { Jumbotron } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      message: 'Fighting...',
    };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;

    
    const xyArray = data.split('\n');
    const rows = xyArray.length;
    const cols = xyArray[0].length;
    
    const units = [];
    const elves = [];
    const goblins = [];
    
    const initUnits = () => {
      for (let rowI = 0; rowI < xyArray.length; rowI++) {
        let row = xyArray[rowI].split('');
        xyArray[rowI] = row;
        for (let colI = 0; colI < row.length; colI++) {
          let char = row[colI];
          if (char === 'G' || char === 'E') {
            const unitId = units.length;
            let unit = {
              type: char,
              pos: [rowI, colI],
              id: unitId,
              pwr: 3,
              hp: 200,
            }
            if (char === 'G') goblins.push(unitId);
            if (char === 'E') elves.push(unitId);
            units.push(unit);
            row[colI] = unit;
          }
        }
      }
    }
    
    let tableRows = [];
    
    const fillCell = char => {
      if (typeof char === 'object') {
        const emo = char.type === 'G' ? '👺' : '🧝';
        return <span>{emo}<span className="font-smallish">{char.hp}</span></span>;
      } else if (char === '#') {
        return '🧗';
      } else if (char === '.') {
        return '🌫';
      }
    }
    
    const draw = () => {
      tableRows = [];
      for (let rowI = 0; rowI < xyArray.length; rowI++) {
        let row = xyArray[rowI];
        let trContent = [];
        for (let colI = 0; colI < row.length; colI++) {
          let char = row[colI];
          trContent.push(<td key={`${rowI}${colI}`} className="square-cell">{fillCell(char)}</td>);
        }
        tableRows.push(<tr key={`${rowI}`}>{trContent}</tr>);
      }
    }
    
    const toCoordId = coords => {
      return coords[0] * 1000 + coords[1];
    }
    
    const toCoords = coordId => {
      const strCoordId = coordId.toString();
      const x = Number(strCoordId.substring(0, strCoordId.length - 3) || 0);
      const y = Number(strCoordId.substring(strCoordId.length - 3) || 0);
      return [x, y];
    }
    
    
    
    const findTurnOrder = () => {
      const turnOrder = [];
      for (let rowI = 0; rowI < xyArray.length; rowI++) {
        let row = xyArray[rowI];
        for (let colI = 0; colI < row.length; colI++) {
          let char = row[colI];
          if (typeof char === 'object') {
            turnOrder.push(char.id);
          }
        }
      }
      return turnOrder;
    }
    
    const getDiff = (pos1, pos2) => {
      let xDiff = pos1[0] - pos2[0];
      xDiff = -xDiff > 0 ? -xDiff : xDiff; // make positive
      let yDiff = pos1[1] - pos2[1];
      yDiff = -yDiff > 0 ? -yDiff : yDiff; // make positive
      const diff = xDiff + yDiff;
      return diff;
    }
    
    const holesAroundToArr = (array, pos, checkedArray) => {
    
      const processHole = (adjX, adjY) => {
        const coords = [adjX, adjY];
        if (
          xyArray[adjX][adjY] === '.'
          && (
            !checkedArray
            || checkedArray.indexOf(coords.toString()) === -1
          )
        ) {
          array.push(coords); 
        }
        if (checkedArray) checkedArray.push(coords.toString());
      }
    
      const posX = pos[0];
      const posY = pos[1];
      processHole(posX - 1, posY);
      processHole(posX + 1, posY);
      processHole(posX, posY - 1);
      processHole(posX, posY + 1);
    
    }
    
    const buildEmptyGrid = () => {
      const grid = [];
      const rowTemplate = Array(cols).fill('#');
      for (let i = 0; i < rows; i++) {
        grid.push(rowTemplate.slice(0));
      }
      return grid;
    }
    
    const holesAroundToGrid = (grid, queue, posObj, checkedArray, unitId) => {
      
      const stepCount = posObj.i + 1;
      
      const processHole = (adjX, adjY) => {
        const coords = [adjX, adjY];
        const char = xy(coords);
        if ((
          char === '.'
          || (
            typeof char === 'object'
            && char.id === unitId
          )
        ) && (
          !checkedArray
          || checkedArray.indexOf(coords.toString()) === -1
        )) {
          if (grid[adjX][adjY] === '#') grid[adjX][adjY] = stepCount.toString();
          queue.push({
            i: stepCount,
            pos: coords,
          });
        }
        if (checkedArray) checkedArray.push(coords.toString());
      }
      
      const posX = posObj.pos[0];
      const posY = posObj.pos[1];
      processHole(posX - 1, posY);
      processHole(posX + 1, posY);
      processHole(posX, posY - 1);
      processHole(posX, posY + 1);

    }
    
    const getFirstInRdgOrdr = (array, returnMultiWithIndex) => {
      const order = [];
      for (let i = 0; i < array.length; i++) {
        const pos = array[i];
        order.push({
          el: toCoordId(pos),
          i,
        });
      }
      order.sort((a, b) => a.el - b.el);
      const firsts = [toCoords(order[0].el)];
      if (returnMultiWithIndex) {
        const indices = [order[0].i];
        for (let j = 1; j < order.length; j++) {
          const curr = order[j].el;
          const prev = order[j - 1].el;
          if (curr === prev) {
            firsts.push(toCoords(order[j].el));
            indices.push(order[j].i);
          } else {
            break;
          }
        }
        return { firsts, indices };
      }
      return firsts[0];
    }
  
    const getTargetsInRange = (unitPos, unitType) => {
      const targetsInRange = [];
      const targets = unitType === 'G' ? elves : goblins;
      for (let i = 0; i < targets.length; i++) {
        const targetId = targets[i];
        const target = units[targetId];
        const diff = getDiff(target.pos, unitPos);
        if (diff === 1) {
          targetsInRange.push(target.pos);
        }
      }
      return targetsInRange;
    }
    
    const findHolesNearTargets = unitType => {
      const holes = [];
      const targets = unitType === 'G' ? elves : goblins;
      for (let i = 0; i < targets.length; i++) {
        const targetId = targets[i];
        const target = units[targetId];
        holesAroundToArr(holes, target.pos);
      }
      return holes;
    }
    
    const findReachableSpots = (unitPos, unitId) => {
      // find all reachable holes for unit
      const baseSpots = [];
      const rSpotOA = []; // reachableSpotObjArray
      holesAroundToArr(baseSpots, unitPos);
      for (let i = 0; i < baseSpots.length; i++) {
        const grid = buildEmptyGrid();
        const baseSpot = baseSpots[i];
        const checkedSpots = []; // don't check the same spot twice
        const rSpotQ = [{i: 0, pos: baseSpot}]; // reachableSpotQueue
        while (rSpotQ.length) {
          const currSpot = rSpotQ[0];
          holesAroundToGrid(grid, rSpotQ, currSpot, checkedSpots, unitId); // (grid, queue, stepCount, pos, checkedArray)
          rSpotQ.shift();
        }
        grid[baseSpot[0]][baseSpot[1]] = '0';
        rSpotOA.push({
          baseSpot,
          grid,
        });
      }
      
      return rSpotOA;
    }
    
    const findLowestsByMetric = (array, itemProcessor) => {
      let lowestMetric = null;
      let lowests = [];
      for (let i = 0; i < array.length; i++) {
        const item = array[i];
        const metric = itemProcessor(item);
        if (
          lowestMetric === null
          || metric < lowestMetric
        ) {
          lowests = [];
          lowestMetric = metric;
          lowests.push(item);
        } else if (metric === lowestMetric) {
          lowests.push(item);
        }
      }
      return lowests;
    }
    
    const findLowestSteps = (rSpotOA, holes) => {
      // check each grid
      let lowestSteps = null;
      let lowests = [];
      for (let i = 0; i < holes.length; i++) {
        const hole = holes[i];
        for (let j = 0; j < rSpotOA.length; j++) {
          const rSpotO = rSpotOA[j];
          let steps = rSpotO.grid[hole[0]][hole[1]];
          if (steps !== '#') {
            steps = Number(steps);
            if (
              lowestSteps === null
              || steps < lowestSteps
            ) {
              lowests = [];
              lowestSteps = steps;
              lowests.push({
                baseSpot: rSpotO.baseSpot,
                hole: hole,
              });
            } else if (steps === lowestSteps) {
              lowests.push({
                baseSpot: rSpotO.baseSpot,
                hole: hole,
              });
            }
          }
        }
      }
      return lowests;
    }
    
    const determineHoleAndStep = (lowestSteps) => {
      const holes = [];
      for (let i = 0; i < lowestSteps.length; i++) {
        holes.push(lowestSteps[i].hole);
      }
      const firstsData = getFirstInRdgOrdr(holes, true);
      const stepsForHole = [];
      for (let i = 0; i < firstsData.indices.length; i++) {
        stepsForHole.push(lowestSteps[firstsData.indices[i]]);
      }
      let step;
      if (stepsForHole.length > 1) {
        const steps = [];
        for (let i = 0; i < stepsForHole.length; i++) {
          steps.push(stepsForHole[i].baseSpot);
        }
        step = getFirstInRdgOrdr(steps);
      } else {
        step = stepsForHole[0].baseSpot;
      }
      return step;
    }
    
    const xy = (pos, char) => {
      const x = pos[0];
      const y = pos[1];
      if (char) xyArray[x][y] = char;
      return xyArray[x][y];
    }
    
    const doMove = (unit, chosenPos) => {
      // change xyArray
      xy(unit.pos, '.');
      xy(chosenPos, unit);
      // change unit
      unit.pos = chosenPos;
    }
    
    const getWeakestTargets = targets => {
      const getHP = pos => {
        return xy(pos).hp;
      }
      return findLowestsByMetric(targets, getHP);
    }
    
    
    const doAttack = (unit, targetPos) => {
      const targetId = xy(targetPos).id;
      units[targetId].hp -= unit.pwr;
      if (units[targetId].hp < 1) { //kill
        xy(targetPos, '.');
        const opponents = units[targetId].type === 'E' ? elves : goblins;
        const index = opponents.indexOf(targetId);
        opponents.splice(index, 1);
      }
    }
    
    let fullRounds = 0;
    let gameOver = false;
    
    const runTurn = (unitId) => {
      const unit = units[unitId];
      const unitPos = unit.pos;
      const unitType = unit.type;
      // stop if no targets
      let teammates;
      let opponents;
      if (unitType === 'G') {
        teammates = goblins;
        opponents = elves;
      } else {
        teammates = elves;
        opponents = goblins;
      }
      if (!opponents.length) { // GAME OVER
        
        gameOver = true;
        let winnersHP = 0;
        
        for (let i = 0; i < teammates.length; i++) {
          const winner = units[teammates[i]];
          winnersHP += winner.hp;
        }
        
        const message = `The ${unitType === 'G' ? 'Goblins' : 'Elves'} won a total of ` +
        `${winnersHP} hit points. ${fullRounds} full rounds were completed.` +
        ` The outcome of the battle is ${fullRounds * winnersHP}.`;
        
        this.setState({ message });
        
      } else if (teammates.length && teammates.indexOf(unitId) !== -1) { // not dead
        let targetsInRange = getTargetsInRange(unitPos, unitType);
        if (!targetsInRange.length) { //move
          const holes = findHolesNearTargets(unitType);
          // console.log({holes});
          const rSpotOA = findReachableSpots(unitPos, unitId);
          if (rSpotOA.length) {
            const lowestSteps = findLowestSteps(rSpotOA, holes);
            // console.log({lowestSteps});
            if (lowestSteps.length) {
              const newPos = determineHoleAndStep(lowestSteps);
              // console.log({step});
              doMove(unit, newPos);
              targetsInRange = getTargetsInRange(newPos, unitType);
            }
          }
        }
        if (targetsInRange.length) { //attack
          const weakestTargets = getWeakestTargets(targetsInRange);
          // if (unitId === 4) console.log({weakestTargets});
          const chosenTarget = getFirstInRdgOrdr(weakestTargets);
          doAttack(unit, chosenTarget);
        }
      }
    }
    
    let startShown = false;
    
    const runRound = () => {
      if (fullRounds === 0 && !startShown) {
        startShown = true;
        draw();
        this.setState({ tableRows, fullRounds });
      } else {
        const turnOrder = findTurnOrder();
        for (let i = 0; i < turnOrder.length; i++) {
          if (!gameOver) runTurn(turnOrder[i]);
        }
        if (!gameOver) fullRounds++; 
        draw();
        this.setState({ tableRows, fullRounds });
      }
      setTimeout(() => {
        if (!gameOver) runRound();
      }, 200);
    }
    
    initUnits();
    
    runRound();

  }

  componentDidMount() {

    try {
      this.runSolution();
    } catch(err) {
      console.log(err);
      this.setState({ hasError: true });
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <table className="my-4 text-center">
          <tbody>
            {this.state.tableRows}
          </tbody>
        </table>
        <div className="mb-4">{`Round ${this.state.fullRounds}`}</div>
        <Jumbotron className="Solution__Jumbotron">
          <h3>What is the outcome of the combat described in your puzzle input?</h3>
          <hr className="my-2" />
          <div>{this.state.message}</div>
        </Jumbotron>
      </div>
    );

  }

}

export default CurrentSolution;
