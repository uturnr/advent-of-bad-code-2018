import React from 'react';
import PuzzleError from '../PuzzleError';
import { Jumbotron } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      content: '',
    };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const emojis = codysData.emojis;
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;
    const dataArr = data.split('\n');

    // get all x, y pairs

    const coords = [];
    let xLow;
    let xHigh;
    let yLow;
    let yHigh;

    for (let string of dataArr) {
      const xyRegex = /(.*?), (.*?)$/g;
      const xy = xyRegex.exec(string);
      console.log(xy);
      const x = Number(xy[1]);
      const y = Number(xy[2]);
      coords.push([x,y]);
      if (!xLow || x < xLow) xLow = x;
      if (!xHigh || x > xHigh) xHigh = x;
      if (!yLow || y < yLow) yLow = y;
      if (!yHigh || y > yHigh) yHigh = y;
    }

    // set bounds of grid based on lowest/highest xy

    const width = xHigh - xLow;
    const height = yHigh - yLow;

    const yTemplate = Array(width).fill(0); // columns
    const xyArray = Array(height).fill(null); // rows

    let adjCoords = [];

    coords.forEach((pair, i) => {
      const adjX = pair[0] - xLow;
      const adjY = pair[1] - yLow;
      const adjPair = [adjX, adjY];
      adjCoords.push(adjPair);
    });

    // check each coordinate within grid and find closest pair

    xyArray.forEach((yArray, yIndex) => {
      if (yArray === null) {
        xyArray[yIndex] = yTemplate.slice(0);
      }
      xyArray[yIndex].forEach((value, xIndex) => {
        const totalDiffs = [];
        adjCoords.forEach((pair, coordIndex) => {
          let xDiff = pair[0] - xIndex;
          xDiff = -xDiff > 0 ? -xDiff : xDiff; // make positive
          let yDiff = pair[1] - yIndex;
          yDiff = -yDiff > 0 ? -yDiff : yDiff; // make positive
          const totalDiff = xDiff + yDiff;
          totalDiffs.push({coordIndex, totalDiff});
        });
        totalDiffs.sort((a, b) => a.totalDiff - b.totalDiff)
        let closestPair;
        if (totalDiffs[0].totalDiff === totalDiffs[1].totalDiff) {
          closestPair = 99;
        } else {
          closestPair = totalDiffs[0].coordIndex;
        }
        xyArray[yIndex][xIndex] = closestPair;
      });
    });

    // discard pairs that have a closest pair on the edge

    const pairsToDiscard = [];
    const scoreCount = [];

    xyArray.forEach((yArray, yIndex) => {
      xyArray[yIndex].forEach((value, xIndex) => {
        if (
          yIndex === 0
          || yIndex === xyArray.length - 1
          || xIndex === 0
          || xIndex === xyArray[yIndex].length - 1
        ) {
          pairsToDiscard.push(value);
        }
      });
    });

    xyArray.forEach((yArray, yIndex) => {
      xyArray[yIndex].forEach((value, xIndex) => {
        if (pairsToDiscard.indexOf(value) !== -1) {
          xyArray[yIndex][xIndex] = 99;
        }
        if (!scoreCount[xyArray[yIndex][xIndex]]) {
          scoreCount[xyArray[yIndex][xIndex]] = [xyArray[yIndex][xIndex], 0]
        }
        scoreCount[xyArray[yIndex][xIndex]][1]++;
      });
    });




    scoreCount.sort((a, b) => (b[1] - a[1]));

    // display

    let parsed = [];

    xyArray.forEach((yArray, yIndex) => {
      let row = [];
      xyArray[yIndex].forEach((value, xIndex) => {
        row.push(emojis[value]);
        // `${value.toString().padStart(2, '0')}`
      });
      parsed.push(<div key={yIndex}>{row}</div>);
    });

    const result = `${emojis[scoreCount[1][0]]} has the largest area size: ${scoreCount[1][1]}`

    this.setState({
      content: [
        parsed,
        result,
      ]
    });

  }

  componentDidMount() {

    try {
      this.runSolution();
    } catch(err) {
      console.log(err);
      this.setState({ hasError: true });
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <Jumbotron className="my-4 Solution__Jumbotron">
          <h3>What is the size of the largest area that isn't infinite?</h3>
          <hr className="my-2" />
          <div>{this.state.content[1]}</div>
        </Jumbotron>
        <pre className="my-4 monospace font-small">
          {this.state.content[0]}
        </pre>
      </div>
    );

  }

}

export default CurrentSolution;
