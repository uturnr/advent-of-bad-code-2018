import React from 'react';

import PuzzleError from '../PuzzleError';
import { Jumbotron } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);
    
    this.state = {
      hasError: false,
      answer: 'Thinking...'
    };

    this.tree = [];
    this.q = [];
    this.id = 0;

  }

  componentDidCatch() {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;
    /** @type string */
    // this.str = '^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$'
    this.str = data;

    this.parseDataAndRun();
    
  }

  parseDataAndRun() {
    this.str = this.str.substring(1,this.str.length - 1);
    Promise.resolve().then(() => {
      return this.removeOptional();
    }).then(() => {
      console.log(this.str);

      this.q.push({
        id: this.id,
        parent: null,
        childStr: this.str,
      });
      this.runSteps(this.q[0]);
    });
  }

  removeOptional() {
    const rangeToRemove = [];
    let endFound = false;
    let loopComplete = false;
    for (let i = this.str.length - 1; i > 0; i--) {
      if (i === 1) loopComplete = true;
      const char = this.str[i];
      const beforeChar = this.str[i-1];
      if (endFound && char === '(') {
        rangeToRemove[0] = i;
        this.str = this.str.slice(0, rangeToRemove[0]) + this.str.slice(rangeToRemove[1] + 1);
        return this.removeOptional();
      } else if (char === ')' && beforeChar === '|') {
        rangeToRemove[1] = i;
        endFound = true;
      }
    }
    if (loopComplete) return;
  }

  runSteps({id, parent, childStr}) {
    const matches = /(.*?)\((.+)\)/.exec(childStr);
    const node = {
      id,
      pattern: matches ? matches[1] : childStr,
      parent: parent,
      children: [],
      childStr: matches ? matches[2] : '',
    };
    let cumuTotal = node.pattern.length;
    if (parent !== null) {
      const parentCumu = this.tree[parent].cumuTotal;
      cumuTotal += parentCumu;
    }
    node.cumuTotal = cumuTotal;
    this.tree[id] = node;
    if (matches) { // has kids
      const str = matches[2];
      let currStr = '';
      let bracketLevel = 0;
      for (let i = 0; i < str.length; i++) {
        const char = str[i];
        if (char === '(') {
          bracketLevel++;
        } else if (char === ')') {
          bracketLevel--;
        }
        if (char !== '|' || bracketLevel) {
          currStr += char;
        } else {
          this.pushToQ(id, currStr);
          currStr = '';
        }
      }
      this.pushToQ(id, currStr);
    }
    this.q.shift();
    if (this.q.length) {
      this.runSteps(this.q[0]);
    } else {
      this.findFurthestRoom();
    }
  }

  pushToQ(parentId, currStr) {
    this.id++;
    this.q.push({
      id: this.id,
      parent: parentId,
      childStr: currStr,
    })
    
    this.tree[parentId].children.push(this.id);
  }

  findFurthestRoom() {
    this.tree.sort((a, b) => b.cumuTotal - a.cumuTotal);
    console.log(this.tree);
    this.setState({
      answer: this.tree[0].cumuTotal,
    });
  }

  componentDidMount() {

    if (process.env.NODE_ENV === 'production') {
      try {
        this.runSolution();
      } catch(err) {
        console.error(err);
        this.setState({ hasError: true });
      }
    } else {
      this.runSolution();
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <Jumbotron className="Solution__Jumbotron my-4">
          <h3>What is the largest number of doors you would be required to pass through to reach a room?</h3>
          <hr className="my-2" />
          <div>{this.state.answer}</div>
        </Jumbotron>
      </div>
    );

  }

}

export default CurrentSolution;
