import React from 'react';

import PuzzleError from '../PuzzleError';
import { Jumbotron } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
    };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;
    const dataNum = Number(data);
    
    const elves = [
      {
        currRecScore: 3,
        currRecIndex: 0,
      },
      {
        currRecScore: 7,
        currRecIndex: 1,
      },
    ];
    
    const recScores = [3, 7];
    
    const createNewRecipes = () => {
      let scoreSum = 0;
      elves.forEach(({currRecScore, currRecIndex}) => {
        scoreSum += currRecScore;
      });
      const scoreSumStr = scoreSum.toString();
      let newRecCount = scoreSumStr.length;
      for (let i = 0; i < newRecCount; i++) {
        let newRecScore = scoreSumStr.charAt(i);
        recScores.push(Number(newRecScore));
      }
    }

    const assignCurrRecipes = () => {
      elves.forEach(({currRecScore, currRecIndex}, elfIndex) => {
        const moves = 1 + currRecScore;
        let newRecIndex = currRecIndex + ( moves % recScores.length );
        if (newRecIndex > recScores.length - 1) newRecIndex %= recScores.length;
        const newRecScore = recScores[newRecIndex];
        elves[elfIndex].currRecScore = newRecScore;
        elves[elfIndex].currRecIndex = newRecIndex;
      });
    }
    
    const runRound = () => {
      createNewRecipes();
      assignCurrRecipes();
    }
    
    while (recScores.length < 10 + dataNum) {
      runRound();
    }
    
    const recScoresStr = recScores.join('');
    const answer = recScoresStr.substr(dataNum, 10);
    
    console.log(recScores);
    
    this.setState({ answer });

  }

  componentDidMount() {

    try {
      this.runSolution();
    } catch(err) {
      console.log(err);
      this.setState({ hasError: true });
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <Jumbotron className="Solution__Jumbotron my-4">
          <h3>What are the scores of the ten recipes immediately after the number of recipes in your puzzle input?</h3>
          <hr className="my-2" />
          <div>{this.state.answer}</div>
        </Jumbotron>
      </div>
    );

  }

}

export default CurrentSolution;
