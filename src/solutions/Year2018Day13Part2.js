import React from 'react';

import PuzzleError from '../PuzzleError';
import { Jumbotron } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      answer: 'Driving...',
    };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;
//     const data = `/>-<\\  
// |   |  
// | /<+-\\
// | | | v
// \\>+</ |
//   |   ^
//   \\<->/`;

    const xyArray = data.split('\n');
    const rowLen = xyArray[0].length;
    
    let tracks = {};
    const carts = [];
    const cartSpots = {};
    let drawingBase = data;
    
    const replaceAt = (str, i, replacement) => {
      return str.substr(0, i) + replacement + str.substr(i + replacement.length);
    }

    // loop over data to create objects
    
    for (let row = 0; row < xyArray.length; row++) {
      const currentRow = xyArray[row];
      for (let col = 0, prevChar = ''; col < currentRow.length; col++) {
        const currChar = currentRow[col];
        if (
          currChar === '/'
          && prevChar !== '-'
          && prevChar !== '+'
          && prevChar !== '<'
          && prevChar !== '>'
        ) { // it's a new track. build the track obj
          const trackId = row * 1000 + col;
          const trackArr = ['/'];
          const angles = {};
          let tCurrSegment = 'top'; // segments end with slashes except for left
          let tRow = row; // t for track
          let tCol = col;
          while (tCurrSegment === 'top') {
            tCol++;
            const idCoords = tRow * 1000 + tCol;
            const tCurrChar = xyArray[tRow][tCol];
            if (tCurrChar === '-') trackArr.push(tCurrChar);
            if (
              tCurrChar === '<'
              || tCurrChar === '>'
            ) {
              trackArr.push('-');
              drawingBase = replaceAt(drawingBase, (rowLen + 1) * tRow + tCol, '-');
              carts.push({ 
                loc: idCoords, 
                dir: tCurrChar === '<' ? -1 : 1,
                nextTurn: 0,
                tracks: [trackId],
                trackLocs: [trackArr.length - 1],
              });
              cartSpots[idCoords] = carts.length - 1;
            } 
            if (tCurrChar === '+') trackArr.push(idCoords);
            if (tCurrChar === '\\') {
              trackArr.push('7');
              angles['7'] = trackArr.length - 1;
              tCurrSegment = 'right';
            }
          }
          while (tCurrSegment === 'right') {
            tRow++;
            const idCoords = tRow * 1000 + tCol;
            const tCurrChar = xyArray[tRow][tCol];
            if (tCurrChar === '|') trackArr.push(tCurrChar);
            if (
              tCurrChar === '^'
              || tCurrChar === 'v'
            ) {
              trackArr.push('|');
              drawingBase = replaceAt(drawingBase, (rowLen + 1) * tRow + tCol, '|');
              carts.push({ 
                loc: idCoords, 
                dir: tCurrChar === '^' ? -1 : 1,
                nextTurn: 0,
                tracks: [trackId],
                trackLocs: [trackArr.length - 1],
              });
              cartSpots[idCoords] = carts.length - 1;
            } 
            if (tCurrChar === '+') trackArr.push(idCoords);
            if (tCurrChar === '/') {
              trackArr.push('J');
              angles['J'] = trackArr.length - 1;
              tCurrSegment = 'bottom';
            }
          }
          while (tCurrSegment === 'bottom') {
            tCol--;
            const idCoords = tRow * 1000 + tCol;
            const tCurrChar = xyArray[tRow][tCol];
            if (tCurrChar === '-') trackArr.push(tCurrChar);
            if (
              tCurrChar === '<'
              || tCurrChar === '>'
            ) {
              trackArr.push('-');
              drawingBase = replaceAt(drawingBase, (rowLen + 1) * tRow + tCol, '-');
              carts.push({ 
                loc: idCoords, 
                dir: tCurrChar === '<' ? 1 : -1,
                nextTurn: 0,
                tracks: [trackId],
                trackLocs: [trackArr.length - 1],
              });
              cartSpots[idCoords] = carts.length - 1;
            } 
            if (tCurrChar === '+') trackArr.push(idCoords);
            if (tCurrChar === '\\') {
              trackArr.push('L');
              angles['L'] = trackArr.length - 1;
              tCurrSegment = 'left';
            }
          }
          while (tCurrSegment === 'left') {
            tRow--;
            const idCoords = tRow * 1000 + tCol;
            const tCurrChar = xyArray[tRow][tCol];
            if (tCurrChar === '|') trackArr.push(tCurrChar);
            if (
              tCurrChar === '^'
              || tCurrChar === 'v'
            ) {
              trackArr.push('|');
              drawingBase = replaceAt(drawingBase, (rowLen + 1) * tRow + tCol, '|');
              carts.push({ 
                loc: idCoords, 
                dir: tCurrChar === '^' ? 1 : -1,
                nextTurn: 0,
                tracks: [trackId],
                trackLocs: [trackArr.length - 1],
              });
              cartSpots[idCoords] = carts.length - 1;
            } 
            if (tCurrChar === '+') trackArr.push(idCoords);
            if (tCurrChar === '/') {
              tCurrSegment = null;
            }
          }
          tracks[trackId] = { trackId, trackArr, angles };
        }
        prevChar = currChar;
      }
    }
    
    // loop over tracks to add trackId and trackLocs to intersections
    // intersections' char will contain [coordsIdOfIntersection, pairTrackId, pairTrackLoc]
    const tempTracks = JSON.parse(JSON.stringify(tracks));
    
    for (const track in tracks) {
      const currTrack = tracks[track];
      currTrack.trackArr.forEach((char, i) => { //eslint-disable-line no-loop-func
        if (typeof char === 'number') {
          const charToMatch = char;
          for (const pairTrack in tracks) {
            const currPairTrack = tracks[pairTrack];
            if (
              currTrack.trackId !== currPairTrack.trackId
              && currPairTrack.trackArr.indexOf(charToMatch) !== -1
            ) {
              const pairTrackId = currPairTrack.trackId;
              const pairTrackLoc = currPairTrack.trackArr.indexOf(charToMatch);
              tempTracks[track].trackArr[i] = [char, pairTrackId, pairTrackLoc];
            }
          }
        }
      });
    }
    
    tracks = tempTracks;
    
    console.log(tracks);
    console.log(JSON.parse(JSON.stringify(carts)));
    
    let answer;
    let drawing;
    
    const _this = this;
    const interval = setInterval(() => runTick(_this), 1);
    
    function runTick(_this) {
      
      if (answer) {
        clearInterval(interval)
      } else {
        
        drawing = drawingBase;
        
        let cartSpotsTasks = [];
        let cartSpotsArr = [];
        
        for (const cartSpot in cartSpots) {
          cartSpotsArr.push(cartSpot);
        }
        
        cartSpotsArr.sort((a, b) => a - b);
        
        for (const cartSpot of cartSpotsArr) {
          const cartId = cartSpots[cartSpot];
          const currCart = carts[cartId];
          if (currCart.tracks.length > 1) { // clean up after turn
            currCart.tracks.pop();
            currCart.trackLocs.pop();
          }
          // determine next loc
          const currLoc = currCart.loc;
          const currTrack = tracks[currCart.tracks[0]];
          const currTrackLoc = currCart.trackLocs[0];
          const currTrackArr = currTrack.trackArr;
          const currChar = currTrackArr[currTrackLoc];
          // if (cartId === 1) console.log({cartId, cartSpot, currChar, currTrack, currTrackLoc});
          const currDir = currCart.dir;
          const currAngles = currTrack.angles;
          let nextTurn = currCart.nextTurn;
          // check if at end of track going right or start or track going left
          let newLoc;
          let newTrackLoc;
          let realDir;
          let newDir;
          // check if currChar is an intersection - if it is determine new direction differently
          let turning = false;
          if (typeof currChar === 'object') {
            // check lastRealDir
            const lastRealDir = currCart.lastRealDir;
            if (nextTurn === 1) { // straight
              nextTurn++;
            } else { 
              turning = true
              // which segment of the track are we entering? top, bottom, left, right?
              let seg;
              if (currTrackLoc < currAngles['7']){
                seg = 't';
              } else if (currTrackLoc < currAngles['J']){
                seg = 'r';
              } else if (currTrackLoc < currAngles['L']){
                seg = 'b';
              } else {
                seg = 'l';
              }
              // left turn
              switch (lastRealDir) {
                case 1:
                  realDir = -1000
                  newDir = seg === 'l' ? 1 : -1;
                  break;
                case 1000:
                  realDir = 1
                  newDir = seg === 't' ? 1 : -1;
                  break;
                case -1:
                  realDir = 1000
                  newDir = seg === 'r' ? 1 : -1;
                  break;
                case -1000:
                  realDir = -1
                  newDir = seg === 'b' ? 1 : -1;
                  break;
                default:
              }
              if (nextTurn === 2) { // right
                realDir = -realDir;
                newDir = -newDir;
                nextTurn = 0;
              } else { // left
                nextTurn++;
              }
              newLoc = currCart.loc + realDir;
              currCart.dir = newDir;
              // find newTrackLoc
              if (
                currTrackLoc === currTrackArr.length - 1
                && newDir === 1
              ) {
                newTrackLoc = 0;
              } else if (
                currTrackLoc === 0
                && newDir === -1
              ) {
                newTrackLoc = currCart.trackLocs.length - 1;
              } else {
                newTrackLoc = currTrackLoc + 1 * newDir;
              }
            }
            currCart.nextTurn = nextTurn;
          }
          if (!turning) {
            if (
              currTrackLoc === currTrackArr.length - 1
              && currDir === 1
            ) {
              realDir = -1000; // up 1
              newTrackLoc = 0;
            } else if (
              currTrackLoc === 0
              && currDir === -1
            ) {
              realDir = 1000; // down 1
              newTrackLoc = currTrackArr.length - 1;
            } else {
              newTrackLoc = currTrackLoc + 1 * currDir;
              if (currTrackLoc < currAngles['7']){
                realDir = 1 * currDir; // right 1
              } else if (currTrackLoc === currAngles['7']){
                realDir = currDir === 1 ? 1000 : -1;
              } else if (currTrackLoc < currAngles['J']){
                realDir = 1000 * currDir; // down 1
              } else if (currTrackLoc === currAngles['J']){
                realDir = currDir === 1 ? -1 : -1000;
              } else if (currTrackLoc < currAngles['L']){
                realDir = -1 * currDir; // left 1
              } else if (currTrackLoc === currAngles['L']){
                realDir = currDir === 1 ? -1000 : 1;
              } else {
                realDir = -1000 * currDir; // up 1
              }
            }
            // invert direction if dir is -1
            newLoc = currCart.loc + realDir;
          }
          // set trackLocs
          currCart.trackLocs[0] = newTrackLoc;
          // check if newTrackLoc is an intersection - if it is modify the tracks, trackLocs
          // determine new direction too
          const newChar = currTrackArr[newTrackLoc];
          if (typeof newChar === 'object') {
            if (nextTurn === 1) {
              // push for posterity
              // will be removed right away
              currCart.tracks.push(newChar[1]); //trackId
              currCart.trackLocs.push(newChar[2]); //trackLoc
            } else {
              currCart.tracks.unshift(newChar[1]); //trackId
              currCart.trackLocs.unshift(newChar[2]); //trackLoc
            }
          }
          currCart.loc = newLoc;
          currCart.lastRealDir = realDir;

          // make a list of changes to make to carSpots after loop is complete
          cartSpotsTasks.push({
            currLoc,
            newLoc,
            cartId,
          });
          
        }
        
        let crashCurrLocs = [];
        
        cartSpotsTasks.forEach(({currLoc, newLoc, cartId}) => {
          if (newLoc in cartSpots) { // check for collision
            // CRASH!
            delete cartSpots[newLoc];
            delete cartSpots[currLoc];
            crashCurrLocs.push(newLoc);
            // throw new Error('stop')
          } else if (crashCurrLocs.indexOf(currLoc) === -1 ) { // skip task for other car in carSpotsTasks
            delete cartSpots[currLoc];
            cartSpots[newLoc] = cartId;
            const strLoc = newLoc.toString();
            const row = Number(strLoc.substring(0, strLoc.length - 3) || 0)
            const col = Number(strLoc.substring(strLoc.length - 3) || 0)
            drawing = replaceAt(drawing, (rowLen + 1) * row + col, '█');
          }
        });
        
        if (Object.keys(cartSpots).length === 1) {
          answer = true
          const strLoc = Object.keys(cartSpots)[0].toString();
          // my weird coord id things have y first I guess
          const y = Number(strLoc.substring(0, strLoc.length - 3) || 0)
          const x = Number(strLoc.substring(strLoc.length - 3) || 0)
          answer = `${x},${y}`;
          _this.setState({
            answer,
          });
        }
        
        _this.setState({ drawing });
        
      }
      
    }

  }

  componentDidMount() {

    try {
      this.runSolution();
    } catch(err) {
      console.log(err);
      this.setState({ hasError: true });
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <pre className="my-4 monospace font-small line-height-1">
          {this.state.drawing}
        </pre>
        <Jumbotron className="Solution__Jumbotron my-4">
          <h3>What is the location of the last cart at the end of the first tick where it is the only cart left?</h3>
          <hr className="my-2" />
          <div>{this.state.answer}</div>
        </Jumbotron>
      </div>
    );

  }

}

export default CurrentSolution;
