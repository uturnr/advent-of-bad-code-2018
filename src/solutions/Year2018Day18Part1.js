import React from 'react';

import PuzzleError from '../PuzzleError';
import { Jumbotron } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);
    
    this.state = {
      hasError: false,
      answer: 'Forestrying...',
    };
    
  }

  componentDidCatch() {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;
// const data = `.#.#...|#.
// .....#|##|
// .|..|...#.
// ..|#.....#
// #.#|||#|#|
// ...#.||...
// .|....|...
// ||...#|.#|
// |.||||..|.
// ...#.|..|.`;

    const dataArr = data.split('\n');
    this.parseData(dataArr);

    let i = 0;
    const runForestry = () => {
      if (i === 0) {
        this.renderGrid(i);
      } else {
        this.changeAcres();
        this.renderGrid(i);
      }
      setTimeout(() => {
        if (i <= 10) {
          runForestry();
          i++;
        } else {
          this.getResourceValue();
        }
      }, 250);
    };

    runForestry();
    
  }

  parseData(dataArr) {
    const grid = [];
    const bounds = {
      topX: dataArr[0].length - 1,
      topY: dataArr.length - 1,
    }
    this.bounds = bounds;
    for (let rowI = 0; rowI < dataArr.length; rowI++) {
      const row = [];
      const rowStr = dataArr[rowI];
      for (let colI = 0; colI < rowStr.length; colI++) {
        const char = rowStr.charAt(colI);
        if (char === '.') row.push('🌱');
        if (char === '|') row.push('🌲');
        if (char === '#') row.push('🏭');
      }
      grid.push(row);
    }
    this.grid = grid;
  }

  changeAcres() {
    const newGrid = [];
    for (let rowI = 0; rowI < this.grid.length; rowI++) {
      const row = this.grid[rowI];
      const newRow = [];
      for (let colI = 0; colI < row.length; colI++) {
        const char = row[colI];
        const spots = this.getSpotsAround(colI, rowI);
        const newChar = this.getNewChar(char, spots);
        newRow.push(newChar);
      }
      newGrid.push(newRow);
    }
    this.grid = newGrid;
  }

  getSpotsAround(x, y) {
    const spots = [];
    for (let xAdj = -1; xAdj <= 1; xAdj++) {
      for (let yAdj = -1; yAdj <= 1; yAdj++) {
        const newX = xAdj + x;
        const newY = yAdj + y;
        const topX = this.bounds.topX;
        const topY = this.bounds.topY;
        if (
          newX <= topX
          && newY <= topY
          && newX >= 0
          && newY >= 0
        ) {
          spots.push([newX, newY]);
        }
      }
    }
    return spots;
  }

  getNewChar(char, spots) {
    let trees = 0;
    let lumberyards = 0;
    let newChar = char;

    for (let i = 0; i < spots.length; i++) {
      const x = spots[i][0];
      const y = spots[i][1];
      const currSpotChar = this.grid[y][x];
      if (char === '🌱') {
        if (currSpotChar === '🌲') trees++;
        if (trees === 3) {
          newChar = '🌲';
          break;
        }
      } else if (char === '🌲') {
        if (currSpotChar === '🏭') lumberyards++;
        if (lumberyards === 3) {
          newChar = '🏭';
          break;
        }
      } else if (char === '🏭') {
        newChar = '🌱';
        if (currSpotChar === '🌲') trees++;
        else if (currSpotChar === '🏭') lumberyards++;
        if (lumberyards >= 2 && trees >= 1) {
          newChar = '🏭';
          break;
        }
      }
    }
    
    return newChar;

  }

  renderGrid(i) {
    let txtGrid = '';
    for (let rowI = 0; rowI < this.grid.length; rowI++) {
      for (let colI = 0; colI < this.grid[rowI].length; colI++) {
        txtGrid += this.grid[rowI][colI];
      }
      txtGrid += '\n';
    }
    this.setState({ txtGrid, i });
  }

  getResourceValue() {
    let trees = 0;
    let lumberyards = 0;
    for (let rowI = 0; rowI < this.grid.length; rowI++) {
      const row = this.grid[rowI];
      for (let colI = 0; colI < row.length; colI++) {
        const char = row[colI];
        if (char === '🌲') trees++;
        else if (char === '🏭') lumberyards++;
      }
    }
    this.setState({ answer: trees * lumberyards });
  }

  componentDidMount() {

    if (process.env.NODE_ENV === 'production') {
      try {
        this.runSolution();
      } catch(err) {
        console.error(err);
        this.setState({ hasError: true });
      }
    } else {
      this.runSolution();
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <Jumbotron className="Solution__Jumbotron my-4">
          <h3>What will the total resource value of the lumber collection area be after 10 minutes?</h3>
          <hr className="my-2" />
          <div>{this.state.answer}</div>
        </Jumbotron>
        <div>{`After ${this.state.i} minutes`}</div>
        <pre className="my-4 monospace line-height-1">
          {this.state.txtGrid}
        </pre>

      </div>
    );

  }

}

export default CurrentSolution;
