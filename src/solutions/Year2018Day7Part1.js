import React from 'react';
import PuzzleError from '../PuzzleError';
import { Jumbotron } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      content: '',
    };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;
    const dataArr = data.split('\n');
    const alphabet = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

    // Part 1

    const steps = {};

    for (const string of dataArr) {
      const prereq = string[5];
      const step = string[36];
      if (!steps[step]) {
        steps[step] = {
          prereqs: [prereq],
          remaining: [prereq],
        }
      } else {
        steps[step].prereqs.push(prereq);
        steps[step].remaining.push(prereq);
      }
    }

    let order = '';
    let allDone = false;

    const prereqComplete = prereq => {
      for (const step in steps) {
        const remaining = steps[step].remaining;
        const i = remaining.indexOf(prereq);
        if (i !== -1) {
          remaining.splice(i, 1);
        }
      }
    }

    while(!allDone) {
      for (const letter of alphabet) {
        if (
          order.indexOf(letter) === -1
          && (
            !steps[letter]
            || steps[letter].remaining.length === 0
          )
        ) {
          order += letter;
          prereqComplete(letter);
          break;
        }
      }
      if (order.length === 26) {
        allDone = true;
      }
    }

    this.setState({
      content: `The proper order is ${order}! 🌈`
    });

  }

  componentDidMount() {

    try {
      this.runSolution();
    } catch(err) {
      console.log(err);
      this.setState({ hasError: true });
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <Jumbotron className="my-4 Solution__Jumbotron">
          <h3>In what order should the steps in your instructions be completed?</h3>
          <hr className="my-2" />
          <div>{this.state.content}</div>
        </Jumbotron>
      </div>
    );

  }

}

export default CurrentSolution;
