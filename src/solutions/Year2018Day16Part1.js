import React from 'react';

import PuzzleError from '../PuzzleError';
import { Jumbotron, Table } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);
    
    this.state = {
      hasError: false,
      tableRows: [],
    };
    
    this.ops = {
      
      addr: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] + reg[i[2]];
        return newReg;
      },

      addi: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] + i[2];
        return newReg;
      },

      mulr: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] * reg[i[2]];
        return newReg;
      },

      muli: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] * i[2];
        return newReg;
      },

      banr: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] & reg[i[2]];
        return newReg;
      },

      bani: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] & i[2];
        return newReg;
      },
      
      borr: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] | reg[i[2]];
        return newReg;
      },

      bori: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] | i[2];
        return newReg;
      },
      
      setr: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]];
        return newReg;
      },

      seti: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = i[1];
        return newReg;
      },
      
      gtir: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = i[1] > reg[i[2]] ? 1 : 0;
        return newReg;
      },

      gtri: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] > i[2] ? 1 : 0;
        return newReg;
      },
      
      gtrr: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] > reg[i[2]] ? 1 : 0;
        return newReg;
      },
      
      eqir: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = i[1] === reg[i[2]] ? 1 : 0;
        return newReg;
      },

      eqri: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] === i[2] ? 1 : 0;
        return newReg;
      },
      
      eqrr: (reg, i) => {
        const newReg = this.copyReg(reg);
        newReg[i[3]] = reg[i[1]] === reg[i[2]] ? 1 : 0;
        return newReg;
      },

    };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
  }
  
  copyReg(reg) {
    return [reg[0], reg[1], reg[2], reg[3]];
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;

    const dataArr = data.split('\n');
    this.parseData(dataArr);
    
    this.evaluateAllSamples();

  }
  
  parseData(dataArr) {
    
    this.samples = [];
    
    let lastType;
    let sample = 0;
    
    const processResult = (result, type) => {
      result.shift()
      result = result.map(Number);
      this.samples[sample][type] = result;
      lastType = type;
    }
    
    for (let i = 0; i < dataArr.length; i++) {
      const line = dataArr[i];
      const char0 = line.charAt(0);
      if (char0 === 'B') {
        this.samples[sample] = {};
        const result = /Before: \[(.+), (.+), (.+), (.+)\]/.exec(line);
        processResult(result, char0.toLowerCase());
      } else if (char0.match(/\d/)) { // a number
        const result = /(.+) (.+) (.+) (.+)/.exec(line);
        processResult(result, 'i');
      } else if (char0 === 'A') {
        const result = /After: {2}\[(.+), (.+), (.+), (.+)\]/.exec(line);
        processResult(result, char0.toLowerCase());
      } else if (lastType === '') { // 2 blank lines in a row
        break;
      } else { // blank line
        sample++;
        lastType = '';
      }
    }
    
  }
  
  evaluateAllSamples() {
    
    const tableRows = [];
    let threeOrMore = 0;
    
    for (let i = 0; i < this.samples.length; i++) {
      const sample = this.samples[i];
      const behavesLike = this.tryAllOps(sample);
      if (behavesLike.length >= 3) threeOrMore++;
      tableRows.push(
        <tr key={`${i}`}>
          <td>{sample.b.toString()}</td>
          <td>{sample.i.toString()}</td>
          <td>{sample.a.toString()}</td>
          <td>{behavesLike.toString()}</td>
        </tr>
      );
    }
    
    this.setState({ tableRows, threeOrMore });
    
  }
  
  tryAllOps(sample) {
    
    const behavesLike = [];
    for (const op in this.ops) {
      const after = this.ops[op](sample.b, sample.i);
      if (after.toString() === sample.a.toString()) {
        behavesLike.push(op);
      }
    }
    
    return behavesLike;
    
  }

  componentDidMount() {

    try {
      this.runSolution();
    } catch(err) {
      console.log(err);
      this.setState({ hasError: true });
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <Jumbotron className="Solution__Jumbotron my-4">
          <h3>How many samples in your puzzle input behave like three or more opcodes?</h3>
          <hr className="my-2" />
          <div>{this.state.threeOrMore}</div>
        </Jumbotron>
        <Table className="my-4" size="sm" striped hover>
          <thead>
            <tr>
              <th>Before</th>
              <th>Instructions</th>
              <th>After</th>
              <th>Possible Opcodes</th>
            </tr>
          </thead>
          <tbody>
            {this.state.tableRows}
          </tbody>
        </Table>
      </div>
    );

  }

}

export default CurrentSolution;
