import React from 'react';

import PuzzleError from '../PuzzleError';
import { Jumbotron } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      content: '',
    };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;
    const dataArr = data.split('\n');
    let points = [];

    // organize data, find bounds

    let lowestStartX = 0;
    let lowestStartY = 0;
    let highestStartX = 0;
    let highestStartY = 0;

    for (let i = 0; i < dataArr.length; i++) {
      const dataRegex = /position=<(?: *)([-]?\d*?),(?: *)([-]?\d*?)> velocity=<(?: *)([-]?\d*?),(?: *)([-]?\d*?)>$/g;
      const result = dataRegex.exec(dataArr[i]);
      let startX = Number(result[1]);
      let startY = Number(result[2]);
      if (startX < lowestStartX) lowestStartX = startX;
      if (startY < lowestStartY) lowestStartY = startY;
      if (startX > highestStartX) highestStartX = startX;
      if (startY > highestStartY) highestStartY = startY;
      const point = {
        start: [startX, startY],
        velocity: [Number(result[3]), Number(result[4])],
      }
      points.push(point);
    }

    // make all positions positive, assign currentPos, assign to rows

    let adjX = Math.abs(lowestStartX);
    let adjY = Math.abs(lowestStartY);
    let byRow = [];

    for (let i = 0; i < points.length; i++) {
      let start = points[i].start;
      const posStartX = start[0] + adjX;
      const posStartY = start[1] + adjY;
      points[i].start = [posStartX, posStartY];
      points[i].currentPos = [posStartX, posStartY];
      if (!byRow[posStartY]) byRow[posStartY] = [];
      byRow[posStartY].push(posStartX);
    }

    // draw string grid

    let xSpots = highestStartX + adjX;
    let ySpots = highestStartY + adjY;
    let lastXSpots;
    let lastYSpots;

    let seconds = 0;

    // apply movements and find new bounds

    const revertMovements = () => {

      seconds--;

      let content = [
        `Exactly how many seconds would they have needed to wait for that message to appear?`,
        `${seconds}!!!!!!! 🦄`,
      ];

      this.setState({
        content,
      });
    }

    let allDone = false;

    const applyMovements = () => {
      seconds++;
      byRow = [];
      let lowestCurrX;
      let lowestCurrY;
      let highestCurrX;
      let highestCurrY;
      lastXSpots = xSpots;
      lastYSpots = ySpots;
      for (let i = 0; i < points.length; i++) {
        let currX = points[i].currentPos[0] + points[i].velocity[0];
        let currY = points[i].currentPos[1] + points[i].velocity[1];
        points[i].currentPos = [currX, currY];
        if (!byRow[currY]) byRow[currY] = [];
        byRow[currY].push(currX);
        if (lowestCurrX === undefined || currX < lowestCurrX) lowestCurrX = currX;
        if (lowestCurrY === undefined || currY < lowestCurrY) lowestCurrY = currY;
        if (highestCurrX === undefined || currX > highestCurrX) highestCurrX = currX;
        if (highestCurrY === undefined || currY > highestCurrY) highestCurrY = currY;
      }
      xSpots = highestCurrX - lowestCurrX + 1;
      ySpots = highestCurrY - lowestCurrY + 1;
      if (xSpots > lastXSpots) {
        xSpots = lastXSpots;
        ySpots = lastYSpots;
        return 'fuck';
      } else {
        adjX = lowestCurrX;
        adjY = lowestCurrY;
      }
    }

    while (true) {
      allDone = applyMovements();
      if (allDone === 'fuck') {
        revertMovements();
        break;
      }
    }



  }

  componentDidMount() {

    try {
      this.runSolution();
    } catch(err) {
      console.log(err);
      this.setState({ hasError: true });
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <div className="my-4">
          <Jumbotron className="Solution__Jumbotron">
            <h3>{this.state.content[0]}</h3>
            <hr className="my-2" />
            <div>{this.state.content[1]}</div>
          </Jumbotron>
        </div>
      </div>
    );

  }

}

export default CurrentSolution;
