import React from 'react';

import PuzzleError from '../PuzzleError';
import { Jumbotron } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      content: '',
    };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;
    const serial = Number(data);

    const calcPower = (x, y) => {
      return Math.trunc(((((x + 10) * y) + serial) * (x + 10)) / 100 % 10) - 5;
    }

    const gridArray = [];

    for (let row = 0; row < 300; row++) {
      const y = row + 1;
      const rowArray = [];
      for (let col = 0; col < 300; col++) {
        const x = col + 1;
        const power = calcPower(x, y);
        rowArray.push(power);
      }
      gridArray.push(rowArray);
    }

    console.log(gridArray);

    const papaGridArray = []

    let highestPower9 = 0;
    let winningCoords;

    for (let row = 0; row < 298; row++) {
      const y = row + 1;
      const rowArray = [];
      for (let col = 0; col < 298; col++) {
        const x = col + 1;
        let power9 = 0;
        power9 += gridArray[row][col];
        power9 += gridArray[row][col + 1];
        power9 += gridArray[row][col + 2];
        power9 += gridArray[row + 1][col];
        power9 += gridArray[row + 1][col + 1];
        power9 += gridArray[row + 1][col + 2];
        power9 += gridArray[row + 2][col];
        power9 += gridArray[row + 2][col + 1];
        power9 += gridArray[row + 2][col + 2];
        rowArray.push(power9);
        if (power9 > highestPower9) {
          highestPower9 = power9;
          winningCoords = [x, y];
        }
      }
      papaGridArray.push(rowArray);
    }

    console.log(papaGridArray);
    console.log(winningCoords);

    const drawGrid = () => {
      const xWin = winningCoords[0];
      const yWin = winningCoords[1];
      let jsxGrid = [];
      for (let row = 0; row < 300; row++) {
        if (row + 1 > yWin - 2 && row + 1 < yWin + 4) {
          const rowArray = [];
          for (let col = 0; col < 300; col++) {
            if (col + 1 > xWin - 2 && col + 1 < xWin + 4) {
              if (
                row >= winningCoords[1] - 1
                && row < winningCoords[1] - 1 + 3
                && col >= winningCoords[0] - 1
                && col < winningCoords[0] - 1 + 3
              ) {
                rowArray.push(
                  <span style={{color: 'red', fontWeight: 'bold'}}>
                    {gridArray[row][col].toString().padStart(2)}
                  </span>
                );
              } else {
                rowArray.push(gridArray[row][col].toString().padStart(2));
              }
            }
          }
          jsxGrid.push(rowArray);
        }
      }

      let grid = [];

      jsxGrid.forEach((row, index) => {
        grid.push(<div key={index}>{row}</div>);
      })

      let content = [
        `What is the X,Y coordinate of the top-left fuel cell of the 3x3 square with the largest total power?`,
        `${winningCoords[0]},${winningCoords[1]}! 🦋`,
        grid,
      ];

      this.setState({
        content,
      });
    }

    drawGrid();


  }

  componentDidMount() {

    try {
      this.runSolution();
    } catch(err) {
      console.log(err);
      this.setState({ hasError: true });
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <pre className="my-4 monospace" style={{fontSize: '2rem'}}>
          {this.state.content[2]}
        </pre>
        <Jumbotron className="Solution__Jumbotron">
          <h3>{this.state.content[0]}</h3>
          <hr className="my-2" />
          <div>{this.state.content[1]}</div>
        </Jumbotron>
      </div>
    );

  }

}

export default CurrentSolution;
