import React from 'react';

import PuzzleError from '../PuzzleError';
import { Table, Jumbotron } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      content: '',
    };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;
    const dataArr = data.split('\n');
    const alphabet = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

    const steps = {};

    const buildStepObj = (step, prereq) => {
      let time = 60;
      let adj = alphabet.indexOf(step) + 1;
      time += adj;
      let prereqArray = prereq ? [prereq] : [];
      let remainingArray = prereq ? [prereq] : [];
      return {
        prereqs: prereqArray,
        remaining: remainingArray,
        time,
      }
    }

    for (const string of dataArr) {
      const prereq = string[5];
      const step = string[36];
      if (!steps[step]) {
        steps[step] = buildStepObj(step, prereq);
      } else {
        steps[step].prereqs.push(prereq);
        steps[step].remaining.push(prereq);
      }
    }

    for (const letter of alphabet) {
      if (!steps[letter]) {
        steps[letter] = buildStepObj(letter);
      }
    }

    console.log(steps);

    let seconds = [];
    let second = 0;
    let availableTasks = [];
    let assignedTasks = '';
    let completedTasks = '';
    let workers = new Array(5);
    for (var i = 0; i < 5; i++) {
        workers[i] = {
          task: null,
          remainingSeconds: 0,
        };
    }

    const prereqComplete = prereq => {
      for (const step in steps) {
        const i = steps[step].remaining.indexOf(prereq);
        if (i !== -1) {
          steps[step].remaining.splice(i, 1);
        }
      }
    }

    const getTasks = () => { // check every time one is finished
      console.log('GET MORE');
      for (const letter of alphabet) {
        if (
          assignedTasks.indexOf(letter) === -1
          && steps[letter].remaining.length === 0
        ) {
          availableTasks.push(letter);
        }
      }
    }

    const assignTasks = () => { // assign tasks whenever a task is available and a worker is free
      availableTasks.forEach(task => {
        for(let i = 0; i < workers.length; i++) {
          if (
            workers[i].task === null
          ) {
            workers[i].task = task;
            workers[i].remainingSeconds = steps[task].time;
            assignedTasks += task;
            break;
          }
        }
      });
      availableTasks = [];
    }

    const runSecond = () => {
      assignTasks();
      const secondData = [
        second,
        workers[0].task || '.',
        workers[1].task || '.',
        workers[2].task || '.',
        workers[3].task || '.',
        workers[4].task || '.',
        completedTasks,
      ]
      second++;
      workers.forEach((workerObj, worker) => {
        if (workerObj.task) {
          workers[worker].remainingSeconds--;
          if (workers[worker].remainingSeconds === 0) {
            prereqComplete(workerObj.task);
            completedTasks += workerObj.task;
            workers[worker].task = null;
            getTasks();
          }
        }
      });
      seconds.push(secondData);
    }

    getTasks();
    assignTasks();

    while (completedTasks.length < 26) {
      runSecond();
    }

    runSecond();

    // display

    let rows = [];
    let answer;

    seconds.forEach((secondData, rowIndex) => {
      let cells = [];
      secondData.forEach((cellContent, cellIndex) => {
        cells.push(<td key={cellIndex}>{cellContent}</td>);
        if (
          rowIndex === seconds.length - 1
          && cellIndex === 0
        ) {
          answer = cellContent;
        }
      });
      rows.push(<tr key={rowIndex}>{cells}</tr>);
    });

    this.setState({
      content: [
        rows,
        answer,
      ],
    });


  }

  componentDidMount() {

    try {
      this.runSolution();
    } catch(err) {
      console.log(err);
      this.setState({ hasError: true });
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <Table className="my-4" size="sm" striped hover>
          <thead>
            <tr>
              <th>Second</th>
              <th>Worker 1</th>
              <th>Worker 2</th>
              <th>Worker 3</th>
              <th>Worker 4</th>
              <th>Worker 5</th>
              <th>Done</th>
            </tr>
          </thead>
          <tbody>
            {this.state.content[0]}
          </tbody>
        </Table>
        <Jumbotron className="my-4 Solution__Jumbotron">
          <h3>How many seconds will it take to complete all of the steps?</h3>
          <hr className="my-2" />
          <div>{this.state.content[1]}</div>
        </Jumbotron>
      </div>
    );

  }

}

export default CurrentSolution;
