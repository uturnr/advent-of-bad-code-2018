import React from 'react';

import PuzzleError from '../PuzzleError';
import { Jumbotron, Table } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);
    
    this.state = {
      hasError: false,
      answer: 'Thinking...'
    };

    this.ops = {
      
      addr: (reg, i) => {
        reg[i[3]] = reg[i[1]] + reg[i[2]];
      },

      addi: (reg, i) => {
        reg[i[3]] = reg[i[1]] + i[2];
      },

      mulr: (reg, i) => {
        reg[i[3]] = reg[i[1]] * reg[i[2]];
      },

      muli: (reg, i) => {
        reg[i[3]] = reg[i[1]] * i[2];
      },

      banr: (reg, i) => {
        reg[i[3]] = reg[i[1]] & reg[i[2]];
      },

      bani: (reg, i) => {
        reg[i[3]] = reg[i[1]] & i[2];
      },
      
      borr: (reg, i) => {
        reg[i[3]] = reg[i[1]] | reg[i[2]];
      },

      bori: (reg, i) => {
        reg[i[3]] = reg[i[1]] | i[2];
      },
      
      setr: (reg, i) => {
        reg[i[3]] = reg[i[1]];
      },

      seti: (reg, i) => {
        reg[i[3]] = i[1];
      },
      
      gtir: (reg, i) => {
        reg[i[3]] = i[1] > reg[i[2]] ? 1 : 0;
      },

      gtri: (reg, i) => {
        reg[i[3]] = reg[i[1]] > i[2] ? 1 : 0;
      },
      
      gtrr: (reg, i) => {
        reg[i[3]] = reg[i[1]] > reg[i[2]] ? 1 : 0;
      },
      
      eqir: (reg, i) => {
        reg[i[3]] = i[1] === reg[i[2]] ? 1 : 0;
      },

      eqri: (reg, i) => {
        reg[i[3]] = reg[i[1]] === i[2] ? 1 : 0;
      },
      
      eqrr: (reg, i) => {
        reg[i[3]] = reg[i[1]] === reg[i[2]] ? 1 : 0;
      },

    };

    this.register = [1,0,0,0,0,0];
    this.ipVal = 0;
    this.instructions = [];
    this.potentialPatterns = {};
    this.currentPattern = null;
    this.step = -1;
    this.tableRows = [];
    this.logs = {};
    this.loopStepsByWarp = {};
    this.warpIs = [];
    this.lastWarpEnd = 0;
    this.lastWaysToBreaks = [];
    this.loopBefores = {};
  }

  componentDidCatch() {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;
// const data = `#ip 0
// seti 5 0 1
// seti 6 0 2
// addi 0 1 0
// addr 1 2 3
// setr 1 0 0
// seti 8 0 4
// seti 9 0 5`;

    const dataArr = data.split('\n');
    this.parseData(dataArr);
    this.runProgram();
    
  }

  parseData(dataArr) {
    for (let i  = 0; i < dataArr.length; i++) {
      /** @type {string} */
      const line = dataArr[i];
      if (i === 0) {
        this.ipBoundTo = Number(line.substring(4, line.length));
      } else {
        const result = /(.+) (.+) (.+) (.+)/.exec(line);
        this.instructions.push({
          opname: result[1],
          i: [result[1], Number(result[2]), Number(result[3]), Number(result[4])],
        });
      }
    }
  }

  runProgram(stopAtIP) {
    
    // if (this.step > 10000000000) this.debugHalt = true;
    this.register[this.ipBoundTo] = this.ipVal; // When the instruction pointer is bound to a register, its value is written to that register just before each instruction is executed
    this.step++;
    this.runStep(stopAtIP);
    
  }

  runStep(stopAtIP, flag) {
    if (this.step % 1000 === 0) this.setState({ tableRows: this.tableRows });
    
    const tableIP = this.ipVal.toString();
    const tableBefore = this.register.toString();
    const instruction = this.instructions[this.ipVal];
    const tableOp = `${instruction.opname} ${instruction.i.slice(-3).toString()}`;
    this.ops[instruction.opname](this.register, instruction.i); // run the instruction
    this.ipVal = this.register[this.ipBoundTo] + 1; // the value of that register is written back to the instruction pointer immediately after each instruction finishes execution. Afterward, move to the next instruction by adding one to the instruction pointer.
    const tableAfter = this.register.toString();
    this.tableRows.push(
      <tr key={this.step} className={flag ? 'font-bold' : ''}>
        <td>{this.step}</td>
        <td>{tableIP}</td>
        <td>{tableBefore}</td>
        <td>{tableOp}</td>
        <td>{tableAfter}</td>
      </tr>
    );
    this.logs[this.step] = { tableIP, tableBefore, tableOp, tableAfter };
    let keepGoing = true;
    if (!stopAtIP) {
      keepGoing = this.findPattern(tableIP, this.step);
    } 
    if (Number(tableIP) === stopAtIP) return;
    if (this.ipVal >= this.instructions.length || this.debugHalt) {
      this.setState({ 
        tableRows: this.tableRows,
        answer: this.register[0],
      });
    } else if (keepGoing) {
      setTimeout(() => {
        this.runProgram(stopAtIP);
      });
    }

  }

  findPattern(ipVal, step) {
    console.log('finding pattern');
    
    const pp = this.potentialPatterns;
    
    if (!(ipVal in pp)) {
      pp[ipVal] = { 
        pattern: [],
        matchesFound: 0,
        findingMatch: false,
        pLength: null,
        index: step,
      };
    }

    for (const ipKey in pp) {
      if (
        ipKey === ipVal
        && pp[ipKey].pattern.length
      ) { // a number has been repeated
        if (pp[ipKey].findingMatch === false) { //2nd instance of starting number
          pp[ipKey].findingMatch = 1;
          pp[ipKey].pLength = pp[ipKey].pattern.length;
        } else if (pp[ipKey].findingMatch === 1) { //3rd instance of starting number
          pp[ipKey].findingMatch = 2;
        }
      }
      
      pp[ipKey].pattern.push(ipVal);
      if (pp[ipKey].findingMatch) { // check if added character matches
        const matchableI = pp[ipKey].pattern.length - pp[ipKey].pLength * pp[ipKey].findingMatch - 1;
        if (ipVal === pp[ipKey].pattern[matchableI]) { // char match
          if (pp[ipKey].pattern.length === pp[ipKey].pLength * (pp[ipKey].findingMatch + 1)) { // full match
            pp[ipKey].matchesFound++;
            if (pp[ipKey].matchesFound === 2) {
              this.currentPattern = {
                start: pp[ipKey].index,
                pattern: pp[ipKey].pattern.slice(0, pp[ipKey].pLength),
              };
              console.log(this.currentPattern);
              
              this.potentialPatterns = {};
              this.evaluateLoop();
              return false;
            }
          }
        } else { // no match, reset
          const lastIPKeyI = pp[ipKey].pLength * pp[ipKey].findingMatch; // keep the pattern up to the last ipKey
          const keptPattern = pp[ipKey].pattern.slice(lastIPKeyI, pp[ipKey].pattern.length);
          const keptStepAdj = pp[ipKey].pattern.length - 1 - lastIPKeyI;
          
          pp[ipKey] = {
            pattern: keptPattern,
            matchesFound: 0,
            findingMatch: false,
            pLength: null,
            index: step - keptStepAdj,
          };

        }

      }
    }
    return true;
  }

  evaluateLoop() {
    console.log('found loop');
    const start = this.currentPattern.start;
    const pLength = this.currentPattern.pattern.length;
    const waysToBreak = [];
    for (let i = 0; i < pLength; i++) {
      const ip = this.currentPattern.pattern[i];
      const opname = this.instructions[ip].opname;
      if (opname === 'gtrr' || opname === 'eqrr') { // get three events for each.
        const instruction = this.instructions[ip].i;
        console.log({start});
        console.log({pLength});
        
        
        console.log(this.logs);
        
        const before1 = this.logs[start + i].tableBefore.split(',').map(Number);
        const before2 = this.logs[start + pLength + i].tableBefore.split(',').map(Number);
        const before3 = this.logs[start + pLength * 2 + i].tableBefore.split(',').map(Number);
        const samples = [before1, before2, before3];
        const stepsToBreak = this.getStepsToBreakLoop(instruction, samples, opname, i, pLength, start);
        if (stepsToBreak) {
          stepsToBreak.ip = Number(ip);
          waysToBreak.push(stepsToBreak);
        } 
      }
    }
    waysToBreak.sort((a, b) => a.stepsFromLoopStart - b.stepsFromLoopStart);
    this.lastWaysToBreaks.push(waysToBreak);
    const filteredWaysToBreak = waysToBreak.filter(way => !way.jumpsOver);
    this.logs = {};
    this.breakLoop(start, filteredWaysToBreak[0], pLength);
    
  }

  getOps(samples, opsArr, opsStrArr, factorsArr) {
    for (let i = 0; i < samples[0].length; i++) {
      const one = samples[0][i];
      const two = samples[1][i];
      const three = samples[2][i];
      if (one === two || two === three) {
        opsArr.push(num => num); // fixed
        opsStrArr.push('fixed');
        factorsArr.push(0);
      } else if (three - two === two - one) {
        opsArr.push((num, steps) => num + (two - one) * steps); // sum
        opsStrArr.push('sum');
        factorsArr.push(two - one);
      } else if (three / two === two / one) {
        opsArr.push((num, steps) => num * Math.pow((two / one), steps)); // mult (unused)
        opsStrArr.push('mult');
        factorsArr.push(two / one);
      } else {
        throw new Error('An unexpected operation is performed in the loop.');
      }
    }
  }

  getDiffs(samples, diffs) {
    for (let i = 0; i < samples[0].length; i++) {
      const one = samples[0][i];
      const two = samples[1][i];
      const three = samples[2][i];
      if (three - two === two - one) {
        diffs.push(two - one);
      } else {
        throw new Error('An unexpected operation is performed in the loop.');
      }
    }
  }


  getStepsToBreakLoop(instruction, samples, opname, indexWithinLoop, pLength, start) {
    const opsByReg = [];
    const opsByRegStr = [];
    const factorsByReg = [];

    this.getOps(samples, opsByReg, opsByRegStr, factorsByReg);

    // how many loops until the result will change?
    const inputA = instruction[1];
    const inputAOp = opsByRegStr[inputA];
    const inputB = instruction[2];
    const inputBOp = opsByRegStr[inputB];
    let watchReg;
    let fixedReg;
    let watchOp;
    if (inputAOp === 'fixed' && inputBOp !== 'fixed') {
      fixedReg = inputA;
      watchReg = inputB;
      watchOp = inputBOp;
    } else if (inputAOp !== 'fixed' && inputBOp === 'fixed') {
      fixedReg = inputB;
      watchReg = inputA;
      watchOp = inputAOp;
    } else {
      throw new Error('Fiddlesticks!');
    }

    const watchVal = samples[0][watchReg];
    const watchVal2 = samples[1][watchReg];
    const fixedVal = samples[0][fixedReg];
    const diff = Math.abs(watchVal - fixedVal);
    const diff2 = Math.abs(watchVal2 - fixedVal);
    if (diff <= diff2) return; // values not converging

    const factor = factorsByReg[watchReg];

    const adj = opname === 'gtrr' ? 1 : 0;

    let loops;
    if (watchOp === 'sum') {
      loops = Math.abs((fixedVal + adj - watchVal) / factor);
    } else if (watchOp === 'mult') { // not used. leaving here because it's neat
      loops = Math.ceil(Math.log((fixedVal + adj) / watchVal) / Math.log(factor));
    }

    const jumpsOver = Number.isInteger(loops) ? false : true;

    return {
      before: samples[0],
      start,
      stepsFromLoopStart: loops * pLength + indexWithinLoop,
      indexWithinLoop,
      loops,
      jumpsOver,
      fixedVal,
      watchVal,
      factor,
      samples,
      opname,
      opsByReg,
    };
  }

  breakLoop(start, wayToBreak, pLength) {
    const lwe = this.lastWarpEnd;
    const leadInLen = start - lwe;
    this.warpIs.push(lwe);
    
    
    const lsbw = this.loopStepsByWarp;
    const wi = this.warpIs;
    lsbw[lwe] = wayToBreak.stepsFromLoopStart;
    
    if (
      wi.length >= 4
      && lsbw[wi[wi.length-1]] === lsbw[wi[wi.length-2]]
      && lsbw[wi[wi.length-1]] === lsbw[wi[wi.length-3]]
      && lsbw[wi[wi.length-1]] === lsbw[wi[wi.length-4]]
    ) { //mega warp
      
      this.doMegaWarp(pLength, leadInLen, lsbw[wi[wi.length-1]] + 1);
    } else {
      this.tableRows.splice(this.tableRows.length - pLength * 3, pLength * 3)
      this.tableRows.push(<tr key={`warp${this.step}`}>
        <td>{`+${wayToBreak.stepsFromLoopStart + 1}`}</td><td>TIME WARP</td><td></td><td></td><td></td>
      </tr>);
      this.step = start + wayToBreak.stepsFromLoopStart;
      this.lastWarpEnd = this.step;
      const oldBeforeReg = wayToBreak.before;
      const beforeReg = [];
      for (let i = 0; i < wayToBreak.opsByReg.length; i++) {
        const specialOp = wayToBreak.opsByReg[i];
        const numFromBefore = oldBeforeReg[i];
        beforeReg.push(specialOp(numFromBefore, wayToBreak.loops));
      }
      this.loopBefores[this.lastWarpEnd] = beforeReg.slice(0);
      this.register = beforeReg;
      this.ipVal = this.register[this.ipBoundTo];
      this.runStep();
    }

  }

  doMegaWarp(pLength, leadInLen, loopSteps) {
    console.log('found mega loop');
    
    this.tableRows.splice(this.tableRows.length - pLength * 3, pLength * 3); // remove mini loop rows
    this.tableRows.splice(this.tableRows.length - (leadInLen + 1) * 3, (leadInLen + 1) * 3); // remove mega loop rows
    
    const lastWarpIs = this.warpIs.slice(-3);
    this.lastWaysToBreaks = this.lastWaysToBreaks.slice(-3);
    const lwtb = this.lastWaysToBreaks;
    
    const earlierWaysToBreaks = [];
    let wtbLen = 0
    
    for (let i = 0; i < lwtb.length; i++) {
      const waysToBreak = lwtb[i];
      if (!wtbLen) wtbLen = lwtb[i].length;
      if (wtbLen !== lwtb[i].length) throw new Error ('Inconsistent ways to break');
      const earlierWaysToBreak = [];
      for (let j = 0; j < waysToBreak.length; j++) {
        const wayToBreak = waysToBreak[j];
        if (wayToBreak.jumpsOver) earlierWaysToBreak.push(wayToBreak);
        else break;
      }
      earlierWaysToBreaks.push(earlierWaysToBreak);
    }

    const ewtb1 = earlierWaysToBreaks[0];
    const earlierCount = ewtb1.length;
    if (!earlierCount) throw new Error ('No earlier way to break');
    
    // find how many loops to get an earlier break
    // assuming fixed stays fixed?
    const earlierData = []
    for (let k = 0; k < earlierCount; k++) {
      const samples = [];
      for (let i = 0; i < earlierWaysToBreaks.length; i++) {
        const sample = [];
        const earlierWaysToBreak = earlierWaysToBreaks[i];
        for (let j = 0; j < earlierWaysToBreak.length; j++) {
          const earlierWayToBreak = earlierWaysToBreak[j];
          sample.push(earlierWayToBreak.fixedVal);
          sample.push(earlierWayToBreak.watchVal);
          sample.push(earlierWayToBreak.factor);
        }
        samples.push(sample);
      }
      const opname = ewtb1[k].opname;
      const diffs = [];
      this.getDiffs(samples, diffs);
      earlierData.push({
        index: k,
        samples,
        diffs,
        opname,
      });
    }
    
    for (let i = 0; i < earlierData.length; i++) {
      const earlierDatum = earlierData[i];
      const adj = earlierDatum.opname === 'gtrr' ? 1 : 0;
      const sample1 = earlierDatum.samples[0];
      const diffs = earlierDatum.diffs;
      const startFixedVal = sample1[0];
      const startWatchVal = sample1[1];
      const startFactor = sample1[2];
      const diffFixedVal = diffs[0];
      const diffWatchVal = diffs[1];
      const diffFactor = diffs[2];
      let intFound = false;
      let l = 0;
      while (!intFound) {
        l++;
        if ((startFixedVal + (diffFixedVal * l) + adj - (startWatchVal + (diffWatchVal * l))) % (startFactor + (diffFactor * l)) === 0) {
          intFound = true;
        }
        if (l > 10000000) debugger;
      }
      earlierDatum.loopsToBreakMega = l;
    }
    this.lastWaysToBreaks = [];

    earlierData.sort((a, b) => a.loopsToBreakMega - b.loopsToBreakMega);
    const loops = earlierData[0].loopsToBreakMega;

    const lastRow = this.tableRows.slice(-1)[0];

    const megaLoopStart = Number(lastRow.props.children[0].props.children);

    const samples = [];
    for (let i = 0; i < lastWarpIs.length; i++) {
      const warpI = lastWarpIs[i];
      samples.push(this.loopBefores[warpI]);
    }

    const loopBefore1 = this.loopBefores[lastWarpIs[0]];

    const diffs = [];
    this.getDiffs(samples, diffs);
    
    const newBefore = [];
    for (let i = 0; i < loopBefore1.length; i++) {
      const regVal = loopBefore1[i];
      const diff = diffs[i];
      newBefore.push(regVal + (diff * loops));
    }

    const moves = loopSteps * (loops + 1) + (leadInLen - 1) * loops
    this.step = megaLoopStart + moves;
    this.tableRows.push(<tr key={`warp${this.step}`} className="font-bold">
      <td>{`+${moves}`}</td><td>MEGA TIME WARP</td><td></td><td></td><td></td>
    </tr>);
    
    this.register = newBefore;
    this.ipVal = this.register[this.ipBoundTo];
    this.loopBefores = {};
    this.warpIs = [];
    this.runStep();
    this.setState({ tableRows: this.tableRows });


 

    // debugger;
    
  }

  componentDidMount() {

    if (process.env.NODE_ENV === 'production') {
      try {
        this.runSolution();
      } catch(err) {
        console.error(err);
        this.setState({ hasError: true });
      }
    } else {
      this.runSolution();
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <Jumbotron className="Solution__Jumbotron my-4">
          <h3>What value is left in register 0 when the background process halts?</h3>
          <hr className="my-2" />
          <div>{this.state.answer}</div>
        </Jumbotron>
        <Table className="my-4" size="sm" striped hover>
          <thead>
            <tr>
              <th>Index</th>
              <th>Instruction Pointer</th>
              <th>Before</th>
              <th>Operation</th>
              <th>After</th>
            </tr>
          </thead>
          <tbody>
            {this.state.tableRows}
          </tbody>
        </Table>

      </div>
    );

  }

}

export default CurrentSolution;
