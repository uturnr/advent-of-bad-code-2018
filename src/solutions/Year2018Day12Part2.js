import React from 'react';

import PuzzleError from '../PuzzleError';
import { Jumbotron } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      content: '',
    };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;

    const dataArr = data.split('\n');
    let initialState = dataArr.shift();
    initialState = initialState.substring(15, initialState.length);
    dataArr.shift();
    console.log(initialState);
    console.log(dataArr);

    let plantGuide = [];

    for (let i = 0; i < dataArr.length; i++) {
      if (dataArr[i][9] === '#') {
        plantGuide.push(dataArr[i].substring(0, 5))
      }
    }

    console.log(plantGuide);

    let genCount = 500;
    let originalGenCount = genCount;
    let gens = [];
    let bulkDots = '.'.repeat(genCount + 2);
    const initialExpanded = bulkDots + initialState + bulkDots;
    const len = initialExpanded.length;
    console.log(initialExpanded);

    gens.push(initialExpanded);
    let gensStr = `${initialState}\n`;

    let trimmedGens = ['this represents the 0th gen'];

    let plantCount;

    for (let gen = 1; gen <= genCount; gen++) {
      let lastTrimmedGen = trimmedGens[trimmedGens.length - 1];
      const lastGen = gens[gens.length - 1];
      let thisGen = '..';
      let firstPlantIndex = null;
      let lastPlantIndex = null;
      plantCount = 0;
      for (let i = 2; i < len - 2; i++) {
        const pattern = lastGen.substring(i - 2, i + 3);
        if (plantGuide.indexOf(pattern) !== -1) {
          thisGen += '#';
          plantCount++;
          if (firstPlantIndex === null) firstPlantIndex = i;
          lastPlantIndex = null;
        } else {
          thisGen += '.';
          if (lastPlantIndex === null) lastPlantIndex = i;
        }
      }
      thisGen += '..';
      gens.push(thisGen);

      let trimmedGen = thisGen.substring(firstPlantIndex, lastPlantIndex);
      gensStr += `${trimmedGen}\n`;
      trimmedGens.push(trimmedGen);
      if (lastTrimmedGen === trimmedGen) genCount = gen;
    }

    console.log(gens);

    const lastGen = gens[genCount];
    let plantIdSum = 0;

    for (let i = 0; i < lastGen.length; i++) {
      const plantId = i - (originalGenCount + 2);
      if (lastGen[i] === '#') {
        plantIdSum += plantId;
      }
    }

    const finalPattern = trimmedGens[genCount];
    const secondToLastGen = gens[genCount - 1];
    const patternIndex1 = secondToLastGen.indexOf(finalPattern);
    const patternIndex2 = lastGen.indexOf(finalPattern);
    const direction = patternIndex2 - patternIndex1;
    const remainingGens = 50000000000 - genCount;
    const plantIdAdjustment = plantCount * direction * remainingGens;

    plantIdSum += plantIdAdjustment;

    this.setState({
      content: [
        `${gensStr}\n\n...and so on`,
        plantIdSum
      ],
    })




  }

  componentDidMount() {

    try {
      this.runSolution();
    } catch(err) {
      console.log(err);
      this.setState({ hasError: true });
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <pre className="my-4 monospace">
          {this.state.content[0]}
        </pre>
        <Jumbotron className="Solution__Jumbotron">
          <h3>After fifty billion generations, what is the sum of the numbers of all pots which contain a plant?</h3>
          <hr className="my-2" />
          <div>{this.state.content[1]}</div>
        </Jumbotron>
      </div>
    );

  }

}

export default CurrentSolution;
