import React from 'react';

import PuzzleError from '../PuzzleError';
import { Jumbotron, Progress } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      content: '',
      progress: 0,
    };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;
    const serial = Number(data);

    const calcPower = (x, y) => {
      return Math.trunc(((((x + 10) * y) + serial) * (x + 10)) / 100 % 10) - 5;
    }

    const gridArray = [];

    for (let row = 0; row < 300; row++) {
      const y = row + 1;
      const rowArray = [];
      for (let col = 0; col < 300; col++) {
        const x = col + 1;
        const power = calcPower(x, y);
        rowArray.push(power);
      }
      gridArray.push(rowArray);
    }


    let highestPowerSquare = 0;
    let winningCoordsAndSize;

    const evaluateSquareSize = size => {
      let papaGridArray = []
      for (let row = 0; row < 301 - size; row++) {
        const y = row + 1;
        const rowArray = [];
        for (let col = 0; col < 301 - size; col++) {
          const x = col + 1;
          let powerSquare = 0;
          for (let i = 0; i < size; i++) {
            for (let j = 0; j < size; j++) {
              powerSquare += gridArray[row + i][col + j];
            }
          }
          rowArray.push(powerSquare);
          if (powerSquare > highestPowerSquare) {
            highestPowerSquare = powerSquare;
            winningCoordsAndSize = [x, y, size];
          }
        }
        papaGridArray.push(rowArray);
      }

    }

    const promiseArray = [];

    for (let i = 1; i < 301; i++) {
      const step = new Promise(resolve => {
        setTimeout(() => {
          resolve(evaluateSquareSize(i));
        },0);
      }).then(() => {
        this.setState({ progress: i - 1 });
        const progressBar = document.getElementsByClassName('progress-bar')[0];
        progressBar.style.display='none';
        progressBar.offsetHeight; // eslint-disable-line no-unused-expressions
        progressBar.style.display='block';
      });
      promiseArray.push(step);
    }

    const drawGrid = () => {
      const xWin = winningCoordsAndSize[0];
      const yWin = winningCoordsAndSize[1];
      const size = winningCoordsAndSize[2];
      let jsxGrid = [];
      for (let row = 0; row < 300; row++) {
        if (row + 1 > yWin - 2 && row + 1 < yWin + size + 1) {
          const rowArray = [];
          for (let col = 0; col < 300; col++) {
            if (col + 1 > xWin - 2 && col + 1 < xWin + size + 1) {
              if (
                row >= yWin - 1
                && row < yWin - 1 + size
                && col >= xWin - 1
                && col < xWin - 1 + size
              ) {
                rowArray.push(
                  <span style={{color: 'red', fontWeight: 'bold'}}>
                    {gridArray[row][col].toString().padStart(2)}
                  </span>
                );
              } else {
                rowArray.push(gridArray[row][col].toString().padStart(2));
              }
            }
          }
          jsxGrid.push(rowArray);
        }
      }

      let grid = [];

      jsxGrid.forEach((row, index) => {
        grid.push(<div key={index}>{row}</div>);
      })

      let content = [
        `What is the X,Y,size identifier of the square with the largest total power?`,
        `${winningCoordsAndSize[0]},${winningCoordsAndSize[1]},${winningCoordsAndSize[2]}! 😇`,
        grid,
      ];

      this.setState({
        content,
        progress: 300,
      });
    }

    Promise.all(promiseArray).then(() => {
      drawGrid();
    });

  }

  componentDidMount() {

    try {
      this.runSolution();
    } catch(err) {
      console.log(err);
      this.setState({ hasError: true });
    }

  }

  componentWillUnmount() {

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    let solution;
    if (this.state.progress === 300) {
      solution = (
        <Jumbotron className="Solution__Jumbotron">
          <h3>{this.state.content[0]}</h3>
          <hr className="my-2" />
          <div>{this.state.content[1]}</div>
        </Jumbotron>
      );
    }

    return (
      <div>
        <div className="text-center">{`${this.state.progress} / 300`}</div>
        <Progress value={this.state.progress / 3} style={{transform: 'translateZ(0)'}}/>
        <pre className="my-4 monospace">
          {this.state.content[2]}
        </pre>
        {solution}
      </div>
    );

  }

}

export default CurrentSolution;
