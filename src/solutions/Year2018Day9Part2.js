import React from 'react';

import PuzzleError from '../PuzzleError';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      content: '',
    };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;

    const dataRegex = /(.*?) players; last marble is worth (.*?) points$/g;
    const result = dataRegex.exec(data);
    const playerCount = Number(result[1]);
    const lastMarble = Number(result[2])*100;

    const circle = [];
    const players = Array.from(Array(playerCount).keys(), playerIndex => playerIndex + 1);
    const turns = [];
    const scores = {};
    players.forEach(playerId => {
      scores[playerId] = 0;
    })

    // init
    let playMarble = 0;
    let player = null;
    let value = 0;
    circle.push(playMarble);
    turns.push({
      player,
      value,
    });
    let currentMarbleIndex = 0;
    playMarble++;


    for (; playMarble <= lastMarble; playMarble++) {

      if (playMarble % 100000 === 0) console.log(playMarble); // progress check


      player = players[( playMarble % players.length ) - 1] || players[players.length - 1];
      value = 0;

      if (playMarble % 23 === 0) {
        // yeehaw
        value += playMarble;
        let stealMarbleIndex = currentMarbleIndex - 7;
        if (stealMarbleIndex < 0) {
          stealMarbleIndex += circle.length;
        }
        const stealMarble = circle[stealMarbleIndex];
        value += stealMarble;
        circle.splice(stealMarbleIndex, 1);
        currentMarbleIndex = stealMarbleIndex;
        if (currentMarbleIndex === circle.length) {
          currentMarbleIndex = 0;
        }
        scores[player] += value;

      } else {

        let playMarbleIndex = currentMarbleIndex + 2;
        if (playMarbleIndex >= circle.length + 1) {
          playMarbleIndex -= circle.length;
        }

        // add marble at index
        circle.splice(playMarbleIndex, 0, playMarble);
        currentMarbleIndex = playMarbleIndex;

      }

      turns.push({
        player,
        value,
      })

    }

    console.log(circle);
    console.log(turns);
    console.log(scores);

    let winningPlayer;
    let highestScore = 0;

    for (let player in scores) {
      if (scores[player] > highestScore) {
        highestScore = scores[player];
        winningPlayer = player;
      }
    }

    let content = `What is the winning Elf's score? Player ${winningPlayer} won with ${highestScore} points!`;

    this.setState({
      content,
    });

  }

  componentDidMount() {

    try {
      this.runSolution();
    } catch(err) {
      this.setState({ hasError: true });
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <div className="my-4 monospace">
          {this.state.content}
        </div>
      </div>
    );

  }

}

export default CurrentSolution;
