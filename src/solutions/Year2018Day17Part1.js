import React from 'react';

import PuzzleError from '../PuzzleError';
import { Jumbotron } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);
    
    this.state = {
      hasError: false,
      answer: 'Flowing...',
    };
    
  }

  componentDidCatch() {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;
// const data = `x=495, y=2..7
// y=7, x=495..501
// x=501, y=3..7
// x=498, y=2..4
// x=506, y=1..2
// x=498, y=10..13
// x=504, y=10..13
// y=13, x=498..504`;

    const dataArr = data.split('\n');
    this.parseData(dataArr);
    this.buildBaseGrid();

    let startShown = false;
    const runWater = () => {
      if (!startShown) {
        startShown = true;
        this.renderGrid();
      } else {
        this.fillDown([500, 0]);
        this.renderGrid();
      }
      setTimeout(() => {
        if (!this.flowComplete) {
          runWater();
        } else {
          this.countReachables();
        }
      }, 250);
    };

    runWater();
    
  }

  parseData(dataArr) {
    this.veins = [];
    const bounds = {
      lowestCountedY: null,
      lowestDrawnY: 0,
      highestY: null,
      lowestX: null,
      highestX: null,
      lowestWaterX: 500,
      highestWaterX: 500,
      lowestWaterY: 0,
      highestWaterY: 0,
    } 
    for (let i = 0; i < dataArr.length; i++) {
      const veinInfo = dataArr[i];
      const fixedAxis = veinInfo.charAt(0)
      const result = /[xy]=(\d+), [xy]=(\d+)..(\d+)/.exec(veinInfo);
      const fixed = Number(result[1]);
      const start = Number(result[2]);
      const end = Number(result[3]);
      const range = [];
      if (i === 0) {
        bounds.lowestCountedY = fixedAxis === 'x' ? start : fixed;
        bounds.highestY = fixedAxis === 'x' ? end : fixed;
        bounds.lowestX = fixedAxis === 'x' ? fixed - 1 : start - 1;
        bounds.highestX = fixedAxis === 'x' ? fixed + 1 : end + 1;
      }
      if (fixedAxis === 'x') {
        if (fixed - 1 < bounds.lowestX) bounds.lowestX = fixed - 1;
        else if (fixed + 1 > bounds.highestX) bounds.highestX = fixed + 1;
        if (start < bounds.lowestCountedY) bounds.lowestCountedY = start;
        if (end > bounds.highestY) bounds.highestY = end;
      } else {
        if (fixed < bounds.lowestCountedY) bounds.lowestCountedY = fixed;
        else if (fixed > bounds.highestY) bounds.highestY = fixed;
        if (start - 1 < bounds.lowestX) bounds.lowestX = start - 1;
        if (end + 1 > bounds.highestX) bounds.highestX = end + 1;
      }
      for (let j = 0; j <= end - start; j++) {
        range.push(start + j);
      }
      this.veins.push({
        dir: fixedAxis === 'x' ? 'y' : 'x',
        x: fixedAxis === 'x' ? fixed : range,
        y: fixedAxis === 'y' ? fixed : range,
      });
    }
    this.bounds = bounds;
  }

  buildBaseGrid() {
    const grid = [];
    for (let rowI = 0; rowI <= this.bounds.highestY; rowI++) {
      const row = [];
      for (let colI = 0; colI <= this.bounds.highestX; colI++) {
        row.push('🥪');
      }
      grid.push(row);
    }
    grid[0][500] = '💦';
    this.grid = grid;

    for (let veinI = 0; veinI < this.veins.length; veinI++) {
      const vein = this.veins[veinI];
      const range = vein.dir === 'x' ? vein.x : vein.y;
      const fixed = vein.dir === 'x' ? vein.y : vein.x;
      for (let rangeI = 0; rangeI < range.length; rangeI++) {
        const rangeItem = range[rangeI];
        if (vein.dir === 'x') this.grid[fixed][rangeItem] = '💩';
        else this.grid[rangeItem][fixed] = '💩';
      }
    }
  }

  fillDown(start) {
    const x = start[0];
    let y = start[1];
    let boundaryFound = false;
    while (!boundaryFound) {
      const currChar = this.grid[y][x];
      if (currChar !== '💦') this.grid[y][x] = '💧';
      const nextRowDown = this.grid[y + 1];
      if (!nextRowDown) {
        boundaryFound = true;
      } else {
        const nextCharDown = nextRowDown[x];
        if (nextCharDown === '💩' || nextCharDown === '🌊') {
          boundaryFound = true;
          // render grid?
          this.fillHorizontally([x, y]);
        }
      }
      y++;
    }
  }

  fillHorizontally(start) {
    let x = start[0];
    const y = start[1];
    const positionsToFill = [];

    let leftWallFound = false;
    let leftEdgeFound = false; // dropdown or end of grid
    while (!(leftWallFound || leftEdgeFound)) {
      const currChar = this.grid[y][x];
      const nextCharLeft = this.grid[y][x - 1];
      const nextCharDown = this.grid[y + 1][x];
      if (nextCharDown === '💩' || nextCharDown === '🌊') {
        if (currChar !== '💦') positionsToFill.push([x, y]);
      } else {
        leftEdgeFound = true;
        this.fillDown([x, y]);
      }
      if (!nextCharLeft) {
        leftEdgeFound = true;
      }
      if (nextCharDown === '💩' || nextCharDown === '🌊') {
        if (nextCharLeft === '💩' || nextCharLeft === '🌊') {
          leftWallFound = true;
        }
      }
      x--;
    }

    x = start[0];
    let rightWallFound = false;
    let rightEdgeFound = false;
    while (!(rightWallFound || rightEdgeFound)) {
      const currChar = this.grid[y][x];
      const nextCharRight = this.grid[y][x + 1] || 0;
      const nextCharDown = this.grid[y + 1][x];
      if (nextCharDown === '💩' || nextCharDown === '🌊') {
        if (currChar !== '💦') positionsToFill.push([x, y]);
      } else {
        rightEdgeFound = true;
        this.fillDown([x, y]);
      }
      if (!nextCharRight) {
        rightEdgeFound = true;
      }
      if (nextCharDown === '💩' || nextCharDown === '🌊') {
        if (nextCharRight === '💩' || nextCharRight === '🌊') {
          rightWallFound = true;
        }
      }
      x++;
    }

    const lowestWaterX = this.bounds.lowestWaterX;
    const highestWaterX = this.bounds.highestWaterX;
    const lowestWaterY = this.bounds.lowestWaterY;
    const highestWaterY = this.bounds.highestWaterY;

    for (let i = 0; i < positionsToFill.length; i++) {
      const pos = positionsToFill[i];
      const x = pos[0];
      const y = pos[1];
      const char = (leftWallFound && rightWallFound) ? '🌊' : '💧';
      this.grid[y][x] = char;
      if (x < lowestWaterX) this.bounds.lowestWaterX = x;
      if (x > highestWaterX) this.bounds.highestWaterX = x;
      if (y < lowestWaterY) this.bounds.lowestWaterY = y;
      if (y > highestWaterY) this.bounds.highestWaterY = y;
    }
    
  }

  renderGrid() {
    let txtGrid = '';
    for (let rowI = 0; rowI <= this.bounds.highestWaterY + 1; rowI++) {
      for (let colI = this.bounds.lowestWaterX - 1; colI <= this.bounds.highestWaterX + 1; colI++) {
        if (colI >= 0) txtGrid += this.grid[rowI][colI];
      }
      txtGrid += '\n';
    }
    if (this.state.txtGrid !== txtGrid) {
      this.setState({ txtGrid });
    } else {
      this.flowComplete = true;
    }
  }

  countReachables() {
    let reachables = 0;
    const bds = this.bounds;
    for (let rowI = bds.lowestCountedY; rowI <= bds.highestY; rowI++) {
      const row = this.grid[rowI];
      for (let colI = bds.lowestX; colI <= bds.highestX; colI++) {
        const char = row[colI];
        if (char === '💧' || char === '🌊') reachables++;
      }
    }
    this.setState({ answer: reachables });
  }

  componentDidMount() {

    if (process.env.NODE_ENV === 'production') {
      try {
        this.runSolution();
      } catch(err) {
        console.error(err);
        this.setState({ hasError: true });
      }
    } else {
      this.runSolution();
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <Jumbotron className="Solution__Jumbotron my-4">
          <h3>How many tiles can the water reach within the range of y values in your scan?</h3>
          <hr className="my-2" />
          <div>{this.state.answer}</div>
        </Jumbotron>
        <pre className="my-4 monospace line-height-1 font-super-small">
          {this.state.txtGrid}
        </pre>

      </div>
    );

  }

}

export default CurrentSolution;
