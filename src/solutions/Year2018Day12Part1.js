import React from 'react';

import PuzzleError from '../PuzzleError';
import { Jumbotron } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      content: '',
    };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;

    const dataArr = data.split('\n');
    let initialState = dataArr.shift();
    initialState = initialState.substring(15, initialState.length);
    dataArr.shift();
    console.log(initialState);
    console.log(dataArr);

    let plantGuide = [];

    for (let i = 0; i < dataArr.length; i++) {
      if (dataArr[i][9] === '#') {
        plantGuide.push(dataArr[i].substring(0, 5))
      }
    }

    console.log(plantGuide);

    let gens = [];
    let twentyTwoDots = '.'.repeat(22);
    const initialExpanded = twentyTwoDots + initialState + twentyTwoDots;
    const len = initialExpanded.length;
    console.log(initialExpanded);

    gens.push(initialExpanded);
    let gensStr = `${initialExpanded}\n`;


    for (let gen = 1; gen <= 20; gen++) {
      const lastGen = gens[gens.length - 1];
      let thisGen = '..';
      for (let i = 2; i < len - 2; i++) {
        const pattern = lastGen.substring(i - 2, i + 3);
        if (plantGuide.indexOf(pattern) !== -1) {
          thisGen += '#';
        } else {
          thisGen += '.';
        }
      }
      thisGen += '..';
      gens.push(thisGen);
      gensStr += `${thisGen}\n`;
    }

    const lastGen = gens[20];
    let plantIdSum = 0;

    console.log(lastGen);

    for (let i = 0; i < lastGen.length; i++) {
      const plantId = i - 22;
      if (lastGen[i] === '#') {
        plantIdSum += plantId;
      }
    }

    this.setState({
      content: [
        gensStr,
        plantIdSum
      ],
    })




  }

  componentDidMount() {

    try {
      this.runSolution();
    } catch(err) {
      console.log(err);
      this.setState({ hasError: true });
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <pre className="my-4 monospace">
          {this.state.content[0]}
        </pre>
        <Jumbotron className="Solution__Jumbotron">
          <h3>After 20 generations, what is the sum of the numbers of all pots which contain a plant?</h3>
          <hr className="my-2" />
          <div>{this.state.content[1]}</div>
        </Jumbotron>
      </div>
    );

  }

}

export default CurrentSolution;
