import React from 'react';

import PuzzleError from '../PuzzleError';
import { Jumbotron } from 'reactstrap';

class CurrentSolution extends React.Component {

  constructor(props) {
    super(props);
    
    this.state = {
      hasError: false,
      answer: 'Thinking...'
    };

    this.tree = [];
    this.q = [];
    this.id = 0;

    this.map = {};

  }

  componentDidCatch() {
    this.setState({ hasError: true });
  }

  runSolution() {

    const currentDay = this.props.currentPuzzleStr.substring(0, this.props.currentPuzzleStr.length-5);
    const codysData = require(`../data/${currentDay}`);
    const data = this.props.customPuzzleInput ? this.props.customPuzzleInput : codysData.default;
    /** @type string */
    // this.str = '^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$';
    // this.str = '^ESSWWN(E|NNENN(EESS(WNSE|SSS)|WWWSSSSE(SW|NNNE)))$';
    // this.str = '^E(ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN)$';
    // this.str = '^E(ENNWSWW(NEWS|SSSEEN)(WNSE|EE)(SWEN|NNN))$';
    this.str = data;
    

    this.parseDataAndRun();
    
  }

  parseDataAndRun() {
    this.str = this.str.substring(1,this.str.length - 1);

    Promise.resolve().then(() => {
      return this.parseOptional();
    }).then(() => {
      console.log(this.str);
      this.q.push({
        id: this.id,
        parent: null,
        childStr: this.str,
        start: [0,0],
        movesBefore: 0,
      });
      this.runSteps(this.q[0]);
    });


  }

  parseOptional() {
    let optionalStartLoc;
    let optionalEndLoc;
    let endFound = false;
    let loopComplete = false;
    for (let i = this.str.length - 1; i > 0; i--) {
      if (i === 1) loopComplete = true;
      const char = this.str[i];
      const beforeChar = this.str[i - 1];
      if (endFound && char === '(') {
        optionalStartLoc = i;
        const beforeStart = this.str.slice(0, optionalStartLoc);
        const optional = this.str.slice(optionalStartLoc + 1, optionalEndLoc - 1);
        const afterEnd = this.str.slice(optionalEndLoc + 1);
        this.str = beforeStart + '*' + optional + '*' + afterEnd;
        return this.parseOptional();
      } else if (char === ')' && beforeChar === '|') {
        optionalEndLoc = i;
        // find next boundary right
        endFound = true;
      }
    }
    if (loopComplete) return;
  }

  runSteps({id, parent, childStr, start, movesBefore}) {
    const matches = /(.*?)\((.+)\)/.exec(childStr);
    const node = {
      id,
      pattern: matches ? matches[1] : childStr,
      parent: parent,
      children: [],
      childStr: matches ? matches[2] : '',
      start,
      movesBefore,
    };
    const {childStart, childMovesBefore} = this.navMap(start, node.pattern, movesBefore);
    this.tree[id] = node;
    if (matches) { // has kids
      const str = matches[2];
      let currStr = '';
      let bracketLevel = 0;

      for (let i = 0; i < str.length; i++) {
        let char = str[i];
        if (char === '(') {
          bracketLevel++;
        } else if (char === ')') {
          bracketLevel--;
        }
        if (char !== '|' || bracketLevel) {
          currStr += char;
        } else {
          this.pushToQ(id, currStr, childStart, childMovesBefore);
          currStr = '';
        }
      }
      if (currStr) this.pushToQ(id, currStr, childStart, childMovesBefore);
    }
    this.q.shift();
    if (this.q.length) {
      this.runSteps(this.q[0]);
    } else {
      this.find1000DoorPaths();
    }
  }

  pushToQ(parentId, currStr, start, movesBefore) {
    this.id++;
    this.q.push({
      id: this.id,
      parent: parentId,
      childStr: currStr,
      start,
      movesBefore,
    });
    
    this.tree[parentId].children.push(this.id);
  }

  navMap(start, pattern, moves) {
    console.log(start);
    
    let optionalStartMoves;
    const currPos = start.slice(0);
    for (let i = 0; i < pattern.length; i++) {
      const char = pattern[i];
      const nextChar = pattern[i + 1];
      
      let className;
      if (char === 'N') {
        currPos[1]--;
        className = 'btm ';
        moves++;
      } else if (char === 'E') {
        currPos[0]++;
        className = 'lft ';
        moves++;
      } else if (char === 'S') {
        currPos[1]++;
        className = 'top ';
        moves++;
      } else if (char === 'W') {
        currPos[0]--;
        className = 'rgt ';
        moves++;
      }
      if (nextChar === 'N') {
        className += 'top ';
      } else if (nextChar === 'E') {
        className += 'rgt ';
      } else if (nextChar === 'S') {
        className += 'btm ';
      } else if (nextChar === 'W') {
        className += 'lft ';
      }
      if (char === '*' && !optionalStartMoves) { // start optional moves
        optionalStartMoves = moves;
      } else if (char === '*') { // return to normal moves
        moves = optionalStartMoves;
        optionalStartMoves = null;
      } else {
        const x = currPos[0];
        const y = currPos[1];
        if (this.map[y] === undefined) this.map[y] = {};
        if (this.map[y][x] === undefined) this.map[y][x] = {
          moves,
          className,
        };
        else if (moves < this.map[y][x]) {
          this.map[y][x].moves = moves;
          this.map[y][x].className = className;
        }
      }

    }
    const childStart = currPos;
    const childMovesBefore = moves;
    return {childStart, childMovesBefore};
  }

  find1000DoorPaths() {
    let count = 0;
    this.lowestY = null;
    this.highestY = null;
    this.lowestX = null;
    this.highestX = null;
    this.highestSteps = 0;
    for (const rowI in this.map) {
      if (this.map.hasOwnProperty(rowI)) {
        if (this.lowestY === null) this.lowestY = Number(rowI);
        if (this.highestY === null) this.highestY = Number(rowI);
        if (Number(rowI) < this.lowestY) this.lowestY = Number(rowI);
        if (Number(rowI) > this.highestY) this.highestY = Number(rowI);
        const row = this.map[rowI];
        for (const colI in row) {
          if (row.hasOwnProperty(colI)) {
            if (this.lowestX === null) this.lowestX = Number(colI);
            if (this.highestX === null) this.highestX = Number(colI);
            if (Number(colI) < this.lowestX) this.lowestX = Number(colI);
            if (Number(colI) > this.highestX) this.highestX = Number(colI);
            const room = row[colI];
            if (room.moves >= 1000) count++;
            if (room.moves > this.highestSteps) this.highestSteps = room.moves;
          }
        }
      }
    }
    console.log(this.map);
    console.log(this.tree);
    
    this.drawMap();

    this.setState({
      answer: count,
    });
  }

  drawMap() {
    const tableRows = [];
    for (let rowI = this.lowestY; rowI <= this.highestY; rowI++) {
      const row = this.map[rowI];
      const tableRow = [];
      for (let colI = this.lowestX; colI <= this.highestX; colI++) {
        const room = row[colI];
        const cellContent = room === undefined ? 'x' : room.moves;
        // const hueRotate = cellContent * 360 / this.highestSteps;
        tableRow.push(
          <td
            key={`td${rowI}-${colI}`}
            className={room && room.className}
          >
            {cellContent}
          </td>
        );
      }
      tableRows.push(<tr key={`tr${rowI}`}>{tableRow}</tr>);
    }
    this.setState({ tableRows });
  }

  componentDidMount() {

    if (process.env.NODE_ENV === 'production') {
      try {
        this.runSolution();
      } catch(err) {
        console.error(err);
        this.setState({ hasError: true });
      }
    } else {
      this.runSolution();
    }

  }

  render() {

    if (this.state.hasError) {
      return <PuzzleError customPuzzleInput={this.props.customPuzzleInput}/>;
    }

    return (
      <div>
        <Jumbotron className="Solution__Jumbotron my-4">
          <h3>How many rooms have a shortest path from your current location that pass through at least 1000 doors?</h3>
          <hr className="my-2" />
          <div>{this.state.answer}</div>
        </Jumbotron>
        <table className="font-small with-borders">
          <tbody>{this.state.tableRows}</tbody>
        </table>
      </div>
    );

  }

}

export default CurrentSolution;
