import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';

export default class Header extends React.Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      navIsOpen: false,
      dropdownIsOpen: false,
    };
  }

  toggle(item) {
    this.setState({
      [item]: !this.state[item],
    });
  }

  render() {
    const puzzleInputName = this.props.customPuzzleInputsOn ? 'Custom' : 'Cody’s';
    return (
      <div className="Header">
        <Navbar color="dark" dark expand="md">
          <NavbarBrand href={`${process.env.PUBLIC_URL}/`}>Advent of Code</NavbarBrand>
          <NavbarToggler onClick={() => this.toggle('navIsOpen')} />
          <Collapse isOpen={this.state.navIsOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink href="https://gitlab.com/uturnr/advent-of-bad-code">GitLab</NavLink>
              </NavItem>
              <Dropdown isOpen={this.state.dropdownIsOpen} toggle={() => this.toggle('dropdownIsOpen')} nav inNavbar>
                <DropdownToggle nav caret>
                  {`Using ${puzzleInputName} Puzzle Inputs`}
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    <div onClick={() => this.props.setCustomPuzzleInputsOn(false)}>
                      Use Cody’s Puzzle Inputs
                    </div>
                  </DropdownItem>
                  <DropdownItem>
                    <div onClick={() => this.props.setCustomPuzzleInputsOn(true)}>
                      Use Custom Puzzle Inputs
                    </div>
                  </DropdownItem>
                </DropdownMenu>
              </Dropdown>
          </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}
