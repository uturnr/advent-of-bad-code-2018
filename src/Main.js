import React, { Component } from 'react';
import Home from './Home';
import Puzzle from './Puzzle';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Container } from 'reactstrap';


class Main extends Component {

  render() {

    return (
      <Router>
        <div className="Main">
          <Container className="my-4 Main__container mw-100">
            <Route exact path={`${process.env.PUBLIC_URL}/`} component={Home} />
            <Route exact path={`${process.env.PUBLIC_URL}/puzzle`} render={props => (
              <Puzzle
                customPuzzleInputsOn={this.props.customPuzzleInputsOn}
                setCustomPuzzleInputsOn={this.props.setCustomPuzzleInputsOn}
                {...props}
              />
            )} />
          </Container>
        </div>
      </Router>
    );
  }
}

export default Main;
