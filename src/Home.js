import React, { Component } from 'react';
import { Card, Button, ButtonGroup, CardTitle, Row, Col } from 'reactstrap';

class Home extends Component {

  render() {

    let years = [2019, 2018];

    let daysByYear = {
      2019: [
        5,
      ],
      2018: [
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
      ],
    };

    let content = [];

    const buildDaysForYear = (days, year) => {
      const daysArray = [];

      days.forEach(day => {
        daysArray.push(
          <Col key={year + day} className="my-2">
            <Card body>
              <CardTitle>Day {day}</CardTitle>
              <ButtonGroup>
                <Button onClick={() => this.props.history.push(`${process.env.PUBLIC_URL}/puzzle?id=Year${year}Day${day}Part1`)}>Part 1</Button>
                <Button onClick={() => this.props.history.push(`${process.env.PUBLIC_URL}/puzzle?id=Year${year}Day${day}Part2`)}>Part 2</Button>
              </ButtonGroup>
            </Card>
          </Col>
        );
      });

      return daysArray;
    }
    
    years.forEach(year => {
      content.push(<h1 key={year} className="container mw-100">{year}</h1>);
      content.push(buildDaysForYear(daysByYear[year], year));
    });

    return (
      <div className="Home">
        <Row>
          {content}
        </Row>
      </div>
    );
  }
}

export default Home;
