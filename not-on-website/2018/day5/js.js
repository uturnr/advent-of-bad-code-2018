// Part 1

// var pageContent = document.getElementById('data');
//
// let pairsAvailable = true;
//
// const dataArray = data.split('');
// console.log(dataArray);
//
// while(pairsAvailable) {
//
//   for (let i = 0; i < dataArray.length - 1; i++) {
//     const char1 = dataArray[i];
//     const lowerChar1 = char1.toLowerCase();
//     const char2 = dataArray[i + 1];
//     const lowerChar2 = char2.toLowerCase();
//
//     if (lowerChar1 === lowerChar2) { // same letter
//       let char1IsLower = char1 === lowerChar1 ? true : false;
//       let char2IsLower = char2 === lowerChar2 ? true : false;
//       if (char1IsLower !== char2IsLower) { // they react
//         console.log(`${char1}${char2}`);
//         dataArray.splice(i, 2);
//         break;
//       }
//     }
//
//     if (i === dataArray.length - 2) {
//       console.log(dataArray[i]);
//       console.log('last one');
//       console.log(`${dataArray.length} units remain.`);
//       const parsed =  dataArray.join('');
//       pageContent.innerHTML = parsed;
//       pairsAvailable = false;
//       break;
//     }
//   }
//
// }

// Part 2

var pageContent = document.getElementById('data');

let lowestResult;
let winningLetter;

alphabet.forEach(letter => {

  let pairsAvailable = true;
  const dataArray = data.split('');

  while(pairsAvailable) {

    for (let i = 0; i < dataArray.length - 1; i++) {
      const char1 = dataArray[i];
      const lowerChar1 = char1.toLowerCase();
      if (lowerChar1 === letter) {
        dataArray.splice(i, 1);
        break;
      }
      const char2 = dataArray[i + 1];
      const lowerChar2 = char2.toLowerCase();

      if (lowerChar1 === lowerChar2) { // same letter
        let char1IsLower = char1 === lowerChar1 ? true : false;
        let char2IsLower = char2 === lowerChar2 ? true : false;
        if (char1IsLower !== char2IsLower) { // they react
          dataArray.splice(i, 2);
          break;
        }
      }

      if (i === dataArray.length - 2) {
        const result = dataArray.length;
        console.log(`${result} units remain when removing ${letter}.`);
        if (!lowestResult || result < lowestResult) {
          lowestResult = result;
          winningLetter = letter;
        }
        const parsed =  dataArray.join('');
        pageContent.innerHTML = parsed;
        pairsAvailable = false;
        break;
      }
    }

  }

});

console.log(`${winningLetter} has ${lowestResult} results`);
