const yTemplate = Array(1000).fill(0);
const xyArray = Array(1000).fill(null);

const claims = [];

for (string of data) {
  const claimRegex = /\#(.*?)\ /;
  const xRegex = /\@ (.*?)\,/;
  const yRegex = /\,(.*?)\:/;
  const wRegex = /\: (.*?)\x/;
  const hRegex = /\x(.*)/;
  const claim = claimRegex.exec(string)[1];
  const x = Number(xRegex.exec(string)[1]);
  const y = Number(yRegex.exec(string)[1]);
  const w = Number(wRegex.exec(string)[1]);
  const h = Number(hRegex.exec(string)[1]);
  claims[claim] = {x, y, w, h};
  // console.log(string);
  // console.log(claim);
  // console.log(x);
  // console.log(y);
  // console.log(w);
  // console.log(h);

  if (x + w > 1000) console.error('Width Over 1000');
  if (y + h > 1000) console.error('Height Over 1000');

  // turn in to Array of Arrays
  // . x x
  // . x x
  // [[0,1,1][0,1,1]]

  xyArray.forEach((yArray, yIndex) => {
    if (yArray === null) {
      xyArray[yIndex] = yTemplate.slice(0);
    }
    if (yIndex >= y && yIndex < y + h) {
      xyArray[yIndex].forEach((value, xIndex) => {
        if (xIndex >= x && xIndex < x + w) {
          xyArray[yIndex][xIndex]++;
        }
      });
    }
  });
}

// final loop

let overlapCount = 0;
let parsed = '';

xyArray.forEach((yArray, yIndex) => {
  xyArray[yIndex].forEach((value, xIndex) => {
    parsed += value
    if (value > 1) overlapCount++;
  });
  parsed += '\n'
});

var pageContent = document.getElementById('data');
pageContent.innerHTML = parsed;

// Task 1 answer

console.log(`${overlapCount} square inches overlap.`);

// Task 2 - let's loop some more! (Code revised above)

let winningClaim;

claims.forEach((claim, index) => {

  let highestUsage = 0;

  xyArray.forEach((yArray, yIndex) => {
    if (yIndex >= claim.y && yIndex < claim.y + claim.h) {
      xyArray[yIndex].forEach((value, xIndex) => {
        if (xIndex >= claim.x && xIndex < claim.x + claim.w) {
          const usage = xyArray[yIndex][xIndex];
          if (usage > highestUsage) highestUsage = usage;
        }
      });
    }
  });

  if (highestUsage === 1) winningClaim = index;

});

console.log(`Claim ${winningClaim} has no overlap!`);
