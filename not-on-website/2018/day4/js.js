data.sort();
console.log(data);

let currentGuard;
let currentSleepStart;
const sleepStats = {}; // array indices 0 - 59 for minutes, 60 is total
const sleepRegex = /\:(.*?)\]/;

for (const log of data) {
  //Prep
  if (currentGuard && !sleepStats[currentGuard]) {
    sleepStats[currentGuard] = Array(61).fill(0);
  }

  if (log.indexOf('#') !== -1) { //this is a guard log
    const guardRegex = /\#(.*?)\ /;
    currentGuard = guardRegex.exec(log)[1];
  } else if (log.indexOf('fa') !== -1) { // falls asleep
    // loop and get sleep minutes
    currentSleepStart = sleepRegex.exec(log)[1];
  } else if (log.indexOf('wa') !== -1) { // wakes up
    const currentSleepEnd = sleepRegex.exec(log)[1];
    let sleepMinuteCount = 0;
    for (var i = 0; i < 60; i++) {
      if (i >= currentSleepStart && i < currentSleepEnd) {
        sleepStats[currentGuard][i] += 1;
        sleepMinuteCount++;
      }
    }
    sleepStats[currentGuard][60] += sleepMinuteCount;
  }
}

console.log(sleepStats);

let topGuardByMinutes;
let topMinuteSleepCount = 0;
for (const guard in sleepStats) {
  sleepStats[guard].pop();
  const guardMinutes = sleepStats[guard]
  for (minute of guardMinutes) {
    if (minute > topMinuteSleepCount) {
      topMinuteSleepCount = minute;
      topGuardByMinutes = guard;
    }
  }
}
console.log(`Guard ${topGuardByMinutes} slept lots. One minute they slept ${topMinuteSleepCount} times!`);
