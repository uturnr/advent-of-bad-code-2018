let twoCount = 0;
let threeCount = 0;

for (string of data) {

  const charArray = string.split('');
  let charObj = {};
  for (char of charArray) {
    if (!charObj[char]) charObj[char] = 0;
    charObj[char] += 1;
  }

  let hasTwo, hasThree;
  for (char in charObj) {
    if (charObj[char] === 2) hasTwo = true;
    if (charObj[char] === 3) hasThree = true;
  }

  if (hasTwo) twoCount++;
  if (hasThree) threeCount++;
}

// Part 1 Answer

console.log(
  `Of these box IDs, ${twoCount} of them contain a letter which appears` +
  ` exactly twice, and ${threeCount} of them contain a letter which appears` +
  ` exactly three times. Multiplying these together produces a ` +
  `checksum of ${twoCount} * ${threeCount} = ${twoCount * threeCount}.`);

// Part 2

data.sort();

// Compare each string with the next

const removeChar = (string, index) => {
  const part1 = string.substring(0, index);
  const part2 = string.substring(index + 1, string.length);
  return part1 + part2;
}

for (let i = 0; i < data.length - 1; i++) {
  const string1 = data[i];
  const string2 = data[i + 1];
  const charArray1 = string1.split('');
  const charArray2 = string2.split('');

  let differenceCount = 0;
  let differenceIndex;
  for (let j = 0; j < charArray1.length; j++) {
    if (charArray1[j] !== charArray2[j]) {
      differenceCount++;
      differenceIndex = j;
      if (differenceCount > 1) break;
    }
  }
  if (differenceCount === 1) {
    const commonLetters = removeChar(string1, differenceIndex);
    console.log(
      `${string1} and ${string2} are very similar indeed.` +
      ` In fact, the following letters are common between them: ` +
      ` ${commonLetters}`
    );
  }
}
